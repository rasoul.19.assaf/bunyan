import 'package:bunyan/data/repositories/authentication_repository.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/presentation/screens/ForgetPasswordScreen/forget_password_screen.dart';
import 'package:bunyan/presentation/screens/HomeScreen/home_screen.dart';
import 'package:bunyan/presentation/screens/SignUpScreen/sign_up_screen.dart';
import 'package:bunyan/presentation/widgets/my_button_secondary.dart';
import 'package:bunyan/presentation/widgets/my_text_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

String email = '';
String password = '';

class SignInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset :false,
      backgroundColor: Colors.transparent,
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            opacity: 0.5,
            image: AssetImage("assets/images/sign_in_background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 45.h),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(height: 100.h),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.h),
                  child: Text(
                    "Welcome Back",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 45.sp,
                      color: Colors.white,
                      fontFamily: "AbrilFatface",
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.h,
                ),
                MyTextField(
                  hintText: "Username",
                  onChanged: (value) {
                    email = value;
                  },
                ),
                SizedBox(
                  height: 10.h,
                ),
                MyTextField(
                  hintText: "Password",
                  isObscure: true,
                  onChanged: (value) {
                    password = value;
                  },
                ),
                SizedBox(
                  height: 20.h,
                ),
                SizedBox(
                  height: 45.h,
                  child: MyButtonSecondry(
                    text: 'Login',
                    onPressed: () async {
                      print("email: $email");
                      print(password);
                      User result = await AuthenticationRepository.instance
                          .signIn(email, password);
                      if (result != null) {
                        print(result);
                        await FirestoreRepository.instance
                            .initCurrentUser(result);
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomeScreen(),
                          ),
                        );
                      }
                    },
                  ),
                ),
                SizedBox(height: 50.h),
                SizedBox(
                  height: 45.h,
                  child: MyButtonSecondry(
                    text: 'New Register ?',
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterScreen()));
                    },
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgetPassword()),
                      );
                    },
                    child: const Text(
                      "Forget Password? click here",
                      style: TextStyle(
                        color: Colors.indigo,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 50.h),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
