import 'package:bunyan/data/repositories/cloud_storage_repository.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/logic/bloc/commercial_properties_cubit.dart';
import 'package:bunyan/logic/bloc/residential_properties_cubit.dart';
import 'package:bunyan/logic/bloc/vacant_land_properties_cubit.dart';
import 'package:bunyan/models/property/Commercial_property.dart';
import 'package:bunyan/models/property/Residential_property.dart';
import 'package:bunyan/models/property/VacantLand_property.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:bunyan/presentation/screens/ChattingScreen/chatting_screen.dart';
import 'package:bunyan/presentation/screens/ProfileScreen/profile_screen.dart';
import 'package:bunyan/presentation/widgets/my_image_card.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  late List<Widget> cards;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
    cards = [
      MyImageCard(
          property: Property(name: 'test'), title: 'title', price: '1,000'),
      MyImageCard(
          property: Property(name: 'test'), title: 'title', price: '1,000'),
      MyImageCard(
          property: Property(name: 'test'), title: 'title', price: '1,000'),
      MyImageCard(
          property: Property(name: 'test'), title: 'title', price: '1,000'),
      MyImageCard(
          property: Property(name: 'test'), title: 'title', price: '1,000'),
    ];
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(8.0),
              height: 30.h,
              child: TabBar(
                controller: _tabController,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    20.0,
                  ),
                  color: const Color(0xFF8ebcd2),
                ),
                indicatorPadding: EdgeInsets.symmetric(horizontal: 2.w),
                labelColor: Colors.black,
                unselectedLabelColor: Colors.black,
                tabs: const [
                  Tab(
                    text: 'Vacant Land',
                  ),
                  Tab(
                    text: 'Residential',
                  ),
                  Tab(
                    text: 'Commercial',
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  GestureDetector(
                    onTap: () {
                      //DONE: for testing cloud storage only
                      CloudStorageRepository.instance.listFiles();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => TestingScreen(),
                        ),
                      );
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                      child: Icon(
                        Icons.menu,
                        size: 27.sp,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 15.w),
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(
                          left: 8.w, right: 5.w, top: 2.h, bottom: 2.h),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Icon(
                        Icons.search,
                        color: Colors.grey,
                        size: 26.w,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (BuildContext context) =>
                        CommercialPropertiesCubit(),
                  ),
                  BlocProvider(
                    create: (BuildContext context) =>
                        ResidentialPropertiesCubit(),
                  ),
                  BlocProvider(
                    create: (BuildContext context) =>
                        VacantLandPropertiesCubit(),
                  ),
                ],
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    // first tab bar view widget
                    BlocBuilder<VacantLandPropertiesCubit,
                        VacantLandPropertiesState>(
                      builder: (context, state) {
                        if (state is VacantLandPropertiesInitial ||
                            state is VacantLandPropertiesLoading) {
                          return const Center(
                              child: CircularProgressIndicator());
                        } else {
                          return Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              ListView.builder(
                                itemCount: state.vacantLandProperties.length,
                                itemBuilder: (context, index) {
                                  VacantLandProperty property =
                                      state.vacantLandProperties[index];
                                  return MyImageCard(
                                    property: property,
                                    title: property.name,
                                    price: property.price != null
                                        ? property.price.toString()
                                        : '100',
                                    isFavorite: property.isLikedBy(
                                        FirebaseAuth.instance.currentUser!.uid),
                                  );
                                },
                              ),
                              GestureDetector(
                                onTap: () {
                                  context
                                      .read<VacantLandPropertiesCubit>()
                                      .load();
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.refresh,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                    BlocBuilder<ResidentialPropertiesCubit,
                        ResidentialPropertiesState>(
                      builder: (context, state) {
                        if (state is ResidentialPropertiesInitial ||
                            state is ResidentialPropertiesLoading) {
                          return const Center(
                              child: CircularProgressIndicator());
                        } else {
                          return Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              ListView.builder(
                                itemCount: state.residentialProperties.length,
                                itemBuilder: (context, index) {
                                  ResidentialProperty property =
                                      state.residentialProperties[index];
                                  return MyImageCard(
                                      property: property,
                                      title: property.name,
                                      price: property.price != null
                                          ? property.price.toString()
                                          : '100');
                                },
                              ),
                              GestureDetector(
                                onTap: () {
                                  context
                                      .read<ResidentialPropertiesCubit>()
                                      .load();
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.refresh,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                    BlocBuilder<CommercialPropertiesCubit,
                        CommercialPropertiesState>(
                      builder: (context, state) {
                        if (state is CommercialPropertiesInitial ||
                            state is CommercialPropertiesLoading) {
                          return const Center(
                              child: CircularProgressIndicator());
                        } else {
                          return Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              ListView.builder(
                                itemCount: state.commercialProperties.length,
                                itemBuilder: (context, index) {
                                  CommercialProperty property =
                                      state.commercialProperties[index];
                                  return MyImageCard(
                                    property: property,
                                    title: property.name,
                                    price: property.price != null
                                        ? property.price.toString()
                                        : '100',
                                    isFavorite: property.isLikedBy(
                                        FirestoreRepository.currentUser!.id),
                                  );
                                },
                              ),
                              GestureDetector(
                                onTap: () {
                                  context
                                      .read<CommercialPropertiesCubit>()
                                      .load();
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.refresh,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 50.h,
                    child: OutlinedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const ProfileScreen(),
                          ),
                        );
                      },
                      style: ButtonStyle(
                        side: MaterialStateProperty.all(
                            const BorderSide(width: 1, color: Colors.black)),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0))),
                      ),
                      child: Text("Account",
                          style:
                              TextStyle(fontSize: 18.sp, color: Colors.black)),
                    ),
                  ),
                ),
                Expanded(
                  child: SizedBox(
                    height: 50.h,
                    child: OutlinedButton(
                      onPressed: () {
                        /*Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => ChattingScreen(
                              otherUserId: 'test',
                            ),
                          ),
                        );*/
                      },
                      style: ButtonStyle(
                        side: MaterialStateProperty.all(
                            const BorderSide(width: 1, color: Colors.black)),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0))),
                      ),
                      child: Text("Chat with us",
                          style:
                              TextStyle(fontSize: 18.sp, color: Colors.black)),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class TestingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
