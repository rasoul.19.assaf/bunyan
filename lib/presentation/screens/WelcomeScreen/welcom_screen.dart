import 'package:bunyan/core/constants/constants.dart';
import 'package:bunyan/data/repositories/authentication_repository.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/presentation/screens/HomeScreen/home_screen.dart';
import 'package:bunyan/presentation/screens/SignInScreen/sign_in_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  int time = 3000;

  navigateSignIn() async {
    await Future.delayed(Duration(milliseconds: time), () {});
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => SignInScreen()));
  }

  void navigateHome() async {
    await Future.delayed(Duration(milliseconds: time), () {});
    await FirestoreRepository.instance
        .initCurrentUser(FirebaseAuth.instance.currentUser!);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomeScreen()));
  }

  @override
  void initState() {
    super.initState();
    if (AuthenticationRepository.instance.checkSignedIn()) {
      navigateHome();
    } else {
      navigateSignIn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          color: Colors.lightBlue,
          image: DecorationImage(
            opacity: 0.4,
            image: AssetImage("assets/images/welcome.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: getScreenWidth(context) - 80.w,
              height: 60.h,
              child: FittedBox(
                fit: BoxFit.cover,
                child: Text(
                  "BUNYAN",
                  style: TextStyle(
                      fontSize: 75.sp,
                      color: Colors.white,
                      fontFamily: 'LibreBaskerville'),
                ),
              ),
            ),
            SizedBox(
              width: getScreenWidth(context) - 80.w,
              child: FittedBox(
                fit: BoxFit.cover,
                child: Text(
                  "the right broker makes the difference",
                  style: TextStyle(
                      fontSize: 18.sp,
                      color: Colors.white,
                      fontFamily: 'LibreBaskerville'),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
