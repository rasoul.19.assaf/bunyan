import 'package:bunyan/presentation/screens/SignInScreen/sign_in_screen.dart';
import 'package:bunyan/presentation/widgets/verification_code_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CompleteScreen extends StatelessWidget {
  const CompleteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        color: const Color(0xFF8ebcd2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Spacer(flex: 2),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "Complete",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 34.sp,
                ),
              ),
            ),
            SizedBox(
              height: 40.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "You have just confirmed your identity via SMS code and able to sign in your account immediately.",
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 60.h,
            ),
            Icon(
              Icons.check_circle_outline_outlined,
              color: Colors.white,
              size: 200.sp,
            ),
            SizedBox(
              height: 60.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 45.w),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => SignInScreen(),
                    ),
                  );
                },
                child: Text(
                  'Sign in',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.sp,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  elevation: 6,
                  padding: EdgeInsets.symmetric(vertical: 15.h),
                  // shadowColor: Colors.black,
                  primary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide.none,
                  ),
                ),
              ),
            ),
            Spacer()
          ],
        ),
      ),
    );
  }
}

class ForgetPassword extends StatelessWidget {
  late String emailOrPhone;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        color: const Color(0xFF8ebcd2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Spacer(flex: 3),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "Forgot Password ?",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 34.sp,
                ),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "Enter your email address or phone number associated with your account.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16.sp),
              ),
            ),
            SizedBox(
              height: 50.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 45.0),
              child: TextField(
                onChanged: (value) {
                  emailOrPhone = value;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(borderSide: BorderSide.none),
                  fillColor: Colors.white,
                  isDense: false,
                  filled: true,
                  hintText: "Type here",
                ),
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              child: const Text(
                "We'll send you an email or verification code to reset your password",
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  color: Colors.black54,
                ),
              ),
            ),
            Spacer(flex: 2),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 45.w),
              child: ElevatedButton(
                onPressed: () async {
                  /*AuthenticationRepository.instance
                      .forgotPassword(emailOrPhone);
                  */
                  try {
                    /*  await FirebaseAuth.instance.sendPasswordResetEmail(
                        email: 'rasoul.rand.assaf@gmail.com');*/
                    await FirebaseAuth.instance.verifyPhoneNumber(
                      phoneNumber: '+963959613725',
                      verificationFailed: (FirebaseAuthException e) {
                        print('verificationFailed');
                        print(e.toString());
                        if (e.code == 'invalid-phone-number') {
                          print('The provided phone number is not valid.');
                        }
                      },
                      codeSent:
                          (String verificationId, int? resendToken) async {
                        print('codeSent');
                        print('sent');
                        // Update the UI - wait for the user to enter the SMS code

                        String smsCode = '123456';
                        // Create a PhoneAuthCredential with the code
                        PhoneAuthCredential credential =
                            PhoneAuthProvider.credential(
                                verificationId: verificationId,
                                smsCode: smsCode);
                        /*await FirebaseAuth.instance.confirmPasswordReset(
                            code: smsCode, newPassword: 'newPassword');
*/
                        FirebaseAuth.instance.currentUser
                            ?.updatePhoneNumber(credential);
                      },
                      codeAutoRetrievalTimeout: (String verificationId) {},
                      verificationCompleted:
                          (PhoneAuthCredential phoneAuthCredential) {
                        print('3\n\n');
                      },
                    );
                  } catch (e) {
                    print(e.toString());
                  }
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => VerificationScreen(),
                    ),
                  );
                },
                child: Text(
                  'Send',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.sp,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  elevation: 6,
                  padding: EdgeInsets.symmetric(vertical: 15.h),
                  primary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide.none,
                  ),
                ),
              ),
            ),
            Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}

class VerificationScreen extends StatelessWidget {
  TextEditingController codeController = TextEditingController();
  bool hasError = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        color: const Color(0x908ebcd2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Spacer(flex: 3),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "Verification",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 34.sp,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: Text(
                "We just sent you an SMS with 4-digit code, enter this code into field below.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16.sp),
              ),
            ),
            SizedBox(
              height: 50.h,
            ),
            VerificationCodeField(
                textEditingController: codeController,
                hasError: hasError,
                length: 4),
            const Spacer(flex: 3),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 45.w),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => CompleteScreen(),
                    ),
                  );
                },
                child: Text(
                  'Verify',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17.sp,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  elevation: 6,
                  padding: EdgeInsets.symmetric(vertical: 15.h),
                  // shadowColor: Colors.black,
                  primary: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                    side: BorderSide.none,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 10.w, right: 10.w, top: 20.h, bottom: 10.h),
              child: const Text(
                "Don't receive the SMS in one minute?",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black54),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: const Text(
                "send again",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.red, decoration: TextDecoration.underline),
              ),
            ),
            Spacer()
          ],
        ),
      ),
    );
  }
}
