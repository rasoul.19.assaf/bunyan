import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/presentation/screens/PropertiesListScreen/properties_lists_screen.dart';
import 'package:bunyan/presentation/screens/SignInScreen/sign_in_screen.dart';
import 'package:bunyan/presentation/widgets/my_app_bar_secondary.dart';
import 'package:bunyan/presentation/widgets/my_button_primary.dart';
import 'package:bunyan/presentation/widgets/my_info_table.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBarSecondary.getAppBar(
        backButton: () {
          Navigator.pop(context);
        },
        title: FirestoreRepository.currentUser?.type == UserType.business
            ? "Business Account"
            : "Individual Account",
        icon: IconButton(
          icon: const Icon(
            Icons.settings,
            color: Colors.black,
          ),
          tooltip: 'Open shopping cart',
          onPressed: () async {
            try {
              await FirebaseAuth.instance.signOut();
              Navigator.of(context).pop();
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (BuildContext context) => SignInScreen(),
                ),
              );
            } catch (e) {
              print(e.toString());
            }
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  SizedBox(
                    height: 10.h,
                  ),
                  MyInfoTable(),
                  SizedBox(height: 20.h),
                ],
              ),
            ),
            FirestoreRepository.currentUser?.type == UserType.business
                ? Center(
                    child: SizedBox(
                      width: double.infinity,
                      child: MyButtonPrimary(
                        text: 'Properties List',
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PropertiesListScreen(),
                            ),
                          );
                        },
                      ),
                    ),
                  )
                : const SizedBox(),
            SizedBox(
              height: 20.h,
            ),
          ],
        ),
      ),
    );
  }
}
