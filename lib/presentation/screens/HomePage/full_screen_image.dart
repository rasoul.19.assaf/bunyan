import 'package:flutter/material.dart';

class FullScreenImage extends StatelessWidget {
  FullScreenImage({Key? key, required this.image}) : super(key: key);

  Widget image;

  @override
  Widget build(BuildContext context) {
    return RotatedBox(quarterTurns: 1, child: ClipPath(child: image));
  }
}
