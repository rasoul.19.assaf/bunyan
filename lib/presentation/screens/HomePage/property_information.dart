import 'package:bunyan/models/property/property.dart';
import 'package:bunyan/presentation/widgets/album_slider.dart';
import 'package:bunyan/presentation/widgets/my_app_bar_secondary.dart';
import 'package:bunyan/presentation/widgets/my_button_primary.dart';
import 'package:bunyan/presentation/widgets/my_property_info_table.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/constants/constants.dart';
import '../ChattingScreen/chat_with_owner_screen.dart';

class PropertyInformation extends StatefulWidget {
  const PropertyInformation({Key? key, required this.property})
      : super(key: key);

  final Property property;

  @override
  _PropertyInformationState createState() => _PropertyInformationState();
}

class _PropertyInformationState extends State<PropertyInformation> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: MyAppBarSecondary.getAppBar(
        title: widget.property.name,
        backButton: () {
          Navigator.pop(context);
        },
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AlbumSlider(
                heightOfController: 40.h,
                height: getScreenWidth(context) * 0.47.h,
                itemCount: widget.property.imageURLs != null
                    ? widget.property.imageURLs!.length
                    : 1,
                items: getImagesAsList(widget.property.imageURLs ?? [])),
            MyPropertyInfoTable(property: widget.property),
            SizedBox(height: 5.h),
            Padding(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text('Description', style: TextStyle(fontSize: 16.sp))),
            SizedBox(height: 5.h),
            Padding(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text(
                    widget.property.description ??
                        'There is a grocery store on the first floor of the building',
                    style: TextStyle(fontSize: 14.sp))),
            SizedBox(height: 15.h),
            Padding(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text('Property Features',
                    style: TextStyle(
                        fontSize: 16.sp, fontWeight: FontWeight.bold))),
            Padding(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text('the closest places to the property',
                    style: TextStyle(fontSize: 14.sp))),
            Padding(
              padding: EdgeInsets.only(left: 30.w, right: 30.w),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          width: 40.h,
                          color: const Color(0xFF75c3bc),
                          child: Padding(
                              padding: EdgeInsets.all(7.w),
                              child: Image.asset("assets/images/hospital.png",
                                  fit: BoxFit.cover)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Text(
                            widget.property.nearestHospital ?? "- - -",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                        Text(
                          "  Hospital",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          width: 40.h,
                          color: const Color(0xFF939cce),
                          child: Padding(
                              padding: EdgeInsets.all(7.w),
                              child: Image.asset("assets/images/school.png",
                                  fit: BoxFit.cover)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Text(
                            widget.property.nearestSchoolOrCollage ?? "- - -",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                        Text(
                          "  School or Collage",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          width: 40.h,
                          color: const Color(0xFFf47971),
                          child: Padding(
                              padding: EdgeInsets.all(7.w),
                              child: Image.asset("assets/images/market.png",
                                  fit: BoxFit.cover)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Text(
                            widget.property.nearestMarket ?? "- - -",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                        Text(
                          "  Market",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          width: 40.h,
                          color: const Color(0xFF67b6f6),
                          child: Padding(
                              padding: EdgeInsets.all(7.w),
                              child: Image.asset("assets/images/mosque.png",
                                  fit: BoxFit.cover)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Text(
                            widget.property.nearestMosque ?? "- - -",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                        Text(
                          "  Mosque",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.w, bottom: 10.w),
                    child: Row(
                      children: [
                        Container(
                          height: 40.h,
                          width: 40.h,
                          color: const Color(0xFFfcdc8d),
                          child: Padding(
                              padding: EdgeInsets.all(7.w),
                              child: Image.asset("assets/images/marker.png",
                                  fit: BoxFit.cover)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10.w),
                          child: Text(
                            widget.property.cityCenter ?? "- -",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                        Text(
                          "  KM",
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: SizedBox(
                width: getScreenWidth(context) - 40.w,
                child: MyButtonPrimary(
                    text: 'Owner\'s Information',
                    onPressed: () {
                      print(widget.property.toJson());
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) => ChatWithOwner(
                            ownerId: widget.property.ownerId,
                          ),
                        ),
                      );
                    }),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    ));
  }

  List<Widget> getImagesAsList(List<String> urls) {
    if (urls.isEmpty) {
      urls = [
        'https://juliaja.nl/wp-content/uploads/2021/04/noimagefound24-1024x576.png',
      ];
    }
    List<Widget> images = [];
    for (String url in urls) {
      images.add(Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(url),
          ),
        ),
      ));
    }
    return images;
  }
}
