import 'package:bunyan/core/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'Tabs/tab1.dart';
import 'Tabs/tab2.dart';
import 'Tabs/tab3.dart';

class TabForPropertyScreen extends StatefulWidget {
  const TabForPropertyScreen({Key? key}) : super(key: key);

  @override
  State<TabForPropertyScreen> createState() => _TabForPropertyScreenState();
}

class _TabForPropertyScreenState extends State<TabForPropertyScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  List <Widget> tabsItem = [
    const Tab1(),
    const Tab2(),
    const Tab3(),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        TabBar(
          controller: _tabController,
          indicator: BoxDecoration(
            borderRadius: BorderRadius.circular(
              20.0,
            ),
            color: const Color(0xFF8ebcd2),
          ),
          indicatorPadding: EdgeInsets.symmetric(horizontal: 2.w),
          labelColor: Colors.black,
          unselectedLabelColor: Colors.black,
          tabs: const [
            Tab(
              text: 'Vacant Land',
            ),
            Tab(
              text: 'Residential',
            ),
            Tab(
              text: 'Commercial',
            ),
          ],
        ),
        SizedBox(
          height: getScreenHeight(context) - 250.h,
          child: TabBarView(
            controller: _tabController,
            children: [
              tabsItem[0],
              tabsItem[1],
              tabsItem[2],
            ],
          ),
        ),
      ],
    );
  }
}