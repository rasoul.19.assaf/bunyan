import 'package:bunyan/core/constants/constants.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:bunyan/presentation/screens/HomeScreen/home_screen.dart';
import 'package:bunyan/presentation/screens/ProfileScreen/profile_screen.dart';
import 'package:bunyan/presentation/screens/current_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../data/repositories/firestore_repository.dart';
import '../../widgets/my_button_primary.dart';
import '../../widgets/my_stepper.dart';
import '../PropertiesListScreen/properties_lists_screen.dart';
import 'InfoTables/info_table1.dart';
import 'InfoTables/info_table2.dart';
import 'InfoTables/info_table3.dart';

class AddingPropertyStepOneScreen extends StatefulWidget {
  @override
  State<AddingPropertyStepOneScreen> createState() =>
      _AddingPropertyStepOneScreenState();
}

class _AddingPropertyStepOneScreenState
    extends State<AddingPropertyStepOneScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Widget> steps = [InfoTable1(), const InfoTable2(), const InfoTable3()];
  int stepperIndex = 0;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: [
          Row(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(left: 7),
                      child: Icon(
                        Icons.arrow_back_ios_outlined,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      "Back",
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              ),
              const Spacer(),
              MyStepper(
                stepperWidth: getScreenWidth(context) - 200.w,
                stepperIndex: stepperIndex,
                onStepperPressed: (index) {
                  setState(() {
                    stepperIndex = index;
                  });
                },
              ),
              const Spacer(flex: 2),
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(left: 10.w, right: 10.w),
                child: steps[stepperIndex],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 20.h),
            width: getScreenWidth(context) - 60.w,
            child: MyButtonPrimary(
              onPressed: () {
                setState(() {
                  if (stepperIndex == 2) {
                    CurrentRepository.ownerId =
                        FirestoreRepository.currentUser!.id;
                    if (CurrentRepository.name != null) {
                      Property property = CurrentRepository.getProperty();
                      FirestoreRepository.instance.addProperty(
                          property,
                          CurrentRepository.propertyType ??
                              PropertyType.residential);
                      CurrentRepository.cleanStore();
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (BuildContext context) => const HomeScreen(),
                        ),
                      );
                    }
                  } else {
                    stepperIndex++;
                  }
                });
              },
              text: 'Next',
            ),
          ),
        ],
      ),
    ));
  }
}
