import 'package:bunyan/models/property/property.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widgets/input_text_field.dart';
import '../../current_repository.dart';

class InfoTable3 extends StatefulWidget {
  const InfoTable3({Key? key}) : super(key: key);

  @override
  State<InfoTable3> createState() => _InfoTable3State();
}

class _InfoTable3State extends State<InfoTable3> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: 10.h,
            horizontal: 12.w,
          ),
          child: Text(
            'Property Features',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Table(
            border: const TableBorder(
              bottom: BorderSide(color: Colors.black, width: 1),
              horizontalInside: BorderSide(color: Colors.grey, width: 1),
            ),
            defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
            defaultColumnWidth: const IntrinsicColumnWidth(),
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(0.4),
              1: FlexColumnWidth(0.6),
            },
            textBaseline: TextBaseline.alphabetic,
            children: [
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: InputTextField(
                    initValue: CurrentRepository.nearestHospital ?? "",
                    hintTextColor: Colors.grey.shade900,
                    hintText: "Nearest Hospital",
                    fontSizeForHintText: 15.sp,
                    onChange: (value) {
                      CurrentRepository.nearestHospital = value;
                    },
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: InputTextField(
                    initValue: CurrentRepository.nearestSchoolOrCollage ?? "",
                    hintTextColor: Colors.grey.shade900,
                    hintText: "Nearest school or collage",
                    fontSizeForHintText: 15.sp,
                    onChange: (value) {
                      CurrentRepository.nearestSchoolOrCollage = value;
                    },
                  ),
                ),
              ]),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.nearestMarket ?? "",
                      hintTextColor: Colors.grey.shade900,
                      hintText: "Nearest Market",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.nearestMarket = value;
                      },
                    ),
                  ),
                ],
              ),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.nearestMosque ?? "",
                      hintTextColor: Colors.grey.shade900,
                      hintText: "Nearest Mosque",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.nearestMosque = value;
                      },
                    ),
                  ),
                ],
              ),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.cityCenter ?? "",
                      hintTextColor: Colors.grey.shade900,
                      hintText: "City Center (KM)",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        setState(() {
                          CurrentRepository.cityCenter = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
