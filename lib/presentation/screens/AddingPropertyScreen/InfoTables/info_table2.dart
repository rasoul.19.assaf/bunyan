import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../tab_for_property_screen.dart';

class InfoTable2 extends StatelessWidget {
  const InfoTable2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: 10.h,
            horizontal: 12.w,
          ),
          child: Text(
            'What type of property?',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        TabForPropertyScreen()
      ],
    );
  }
}
