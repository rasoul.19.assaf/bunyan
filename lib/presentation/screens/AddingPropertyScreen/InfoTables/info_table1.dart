import 'dart:async';
import 'dart:io';

import 'package:bunyan/core/exceptions/nop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

import '../../../widgets/input_text_field.dart';
import '../../current_repository.dart';

class InfoTable1 extends StatefulWidget {
  InfoTable1({Key? key}) : super(key: key);

  @override
  State<InfoTable1> createState() => _InfoTable1State();
}

class _InfoTable1State extends State<InfoTable1> {
  late Interpreter model;
  List inputs = [];
  double output = 0;

  double price = 0;

  @override
  void initState() {
    getPrice();
  }

  void loadModel() async {
    model = await Interpreter.fromFile(
      File('assets/AI_model/model.tflite'),
    );
    print("model loaded");
    var input0 = [1.23];
    var input1 = [2.43];
    var input2 = [2.43];
    var input3 = [2.43];
    var input4 = [2.43];
    var input5 = [2.43];

    var inputs = [input0, input1, input2, input3, input4, input5];

    var output0 = List<double>.filled(1, 0);

    var outputs = {0: output0};
    model.runForMultipleInputs(inputs, outputs);
    print(outputs);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: 10.h,
            horizontal: 12.w,
          ),
          child: Text(
            'Property price calculator',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Table(
            border: const TableBorder(
              bottom: BorderSide(color: Colors.black, width: 1),
              horizontalInside: BorderSide(color: Colors.grey, width: 1),
            ),
            defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
            defaultColumnWidth: const IntrinsicColumnWidth(),
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(0.4),
              1: FlexColumnWidth(0.6),
            },
            textBaseline: TextBaseline.alphabetic,
            children: [
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: () {
                              getPrice();
                            },
                            child: Text('Calculate')),
                        price == 0
                            ? CircularProgressIndicator()
                            : Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Expanded(
                                  child: Center(
                                      child: Material(
                                    child:
                                        Text(price.ceil().toString() + '000\$'),
                                  )),
                                ),
                              ),
                      ],
                    ),
                  ),
                ],
              ),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: InputTextField(
                    initValue: CurrentRepository.transactionDate ?? "",
                    hintTextColor: Colors.black,
                    hintText: "Transaction Date",
                    fontSizeForHintText: 15.sp,
                    onChange: (value) {
                      CurrentRepository.transactionDate = value;
                    },
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: InputTextField(
                    initValue: CurrentRepository.houseAge ?? "",
                    hintTextColor: Colors.black,
                    hintText: "House Age",
                    fontSizeForHintText: 15.sp,
                    onChange: (value) {
                      CurrentRepository.houseAge = value;
                    },
                  ),
                ),
              ]),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.distanceToMRT ?? "",
                      hintTextColor: Colors.black,
                      hintText: "Distance to the nearest MRT station",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.distanceToMRT = value;
                      },
                    ),
                  ),
                ],
              ),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.numOfStore ?? "",
                      hintTextColor: Colors.black,
                      hintText: "Number of convenience stores",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.numOfStore = value;
                      },
                    ),
                  ),
                ],
              ),
              TableRow(
                children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.title ?? "",
                      hintTextColor: Colors.black,
                      hintText: "Title",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.title = value;
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Opacity(
            opacity: 0.4,
            child: Table(
              border: const TableBorder(
                bottom: BorderSide(color: Colors.black, width: 1),
              ),
              defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
              defaultColumnWidth: const IntrinsicColumnWidth(),
              columnWidths: const <int, TableColumnWidth>{
                0: FlexColumnWidth(0.4),
                1: FlexColumnWidth(0.6),
              },
              textBaseline: TextBaseline.alphabetic,
              children: [
                TableRow(children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.city ?? "",
                      hintTextColor: Colors.grey.shade900,
                      hintText: "City",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.city = value;
                      },
                    ),
                  ),
                ]),
                TableRow(children: [
                  TableCell(
                    verticalAlignment: TableCellVerticalAlignment.middle,
                    child: InputTextField(
                      initValue: CurrentRepository.neighborhood ?? "",
                      hintTextColor: Colors.grey.shade900,
                      hintText: "Neighborhood",
                      fontSizeForHintText: 15.sp,
                      onChange: (value) {
                        CurrentRepository.neighborhood = value;
                      },
                    ),
                  ),
                ]),
                TableRow(
                  children: [
                    TableCell(
                      verticalAlignment: TableCellVerticalAlignment.middle,
                      child: InputTextField(
                        initValue: CurrentRepository.latitude ?? "",
                        hintTextColor: Colors.grey.shade900,
                        hintText: "Latitude",
                        fontSizeForHintText: 15.sp,
                        onChange: (value) {
                          CurrentRepository.latitude = value;
                        },
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      verticalAlignment: TableCellVerticalAlignment.middle,
                      child: InputTextField(
                        initValue: CurrentRepository.longitude ?? "",
                        hintTextColor: Colors.grey.shade900,
                        hintText: "Longitude",
                        fontSizeForHintText: 15.sp,
                        onChange: (value) {
                          CurrentRepository.longitude = value;
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Future<void> getPrice() async {
    setState(() {
      price = 0;
    });
    var price2 = await calculatePrice();
    setState(() {
      price = price2;
    });
  }
}
