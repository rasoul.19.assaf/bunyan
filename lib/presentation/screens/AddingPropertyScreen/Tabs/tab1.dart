import 'package:bunyan/core/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../data/repositories/cloud_storage_repository.dart';
import '../../../../data/repositories/firestore_repository.dart';
import '../../../widgets/error_dialog.dart';
import '../../../widgets/image_dialog.dart';
import '../../../widgets/input_text_field.dart';
import '../../current_repository.dart';

class Tab1 extends StatelessWidget {
  const Tab1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      child: Table(
        border: const TableBorder(
          bottom: BorderSide(color: Colors.black, width: 1),
          horizontalInside: BorderSide(color: Colors.grey, width: 1),
        ),
        defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
        defaultColumnWidth: const IntrinsicColumnWidth(),
        columnWidths: const <int, TableColumnWidth>{
          0: FlexColumnWidth(0.4),
          1: FlexColumnWidth(0.6),
        },
        textBaseline: TextBaseline.alphabetic,
        children: [
          TableRow(children: [
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: InputTextField(
                initValue: CurrentRepository.name ?? "",
                hintTextColor: Colors.grey.shade900,
                hintText: "Name",
                fontSizeForHintText: 15.sp,
                onChange: (value) {
                  CurrentRepository.propertyType = PropertyType.vacantLand;
                  CurrentRepository.name = value;
                },
              ),
            ),
          ]),
          TableRow(children: [
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: InputTextField(
                initValue: CurrentRepository.location ?? "",
                hintTextColor: Colors.grey.shade900,
                hintText: "Location",
                fontSizeForHintText: 15.sp,
                onChange: (value) {
                  CurrentRepository.location = value;
                },
              ),
            ),
          ]),
          TableRow(
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: InputTextField(
                  initValue: CurrentRepository.totalArea ?? "",
                  hintTextColor: Colors.grey.shade900,
                  hintText: "The total area (m2)",
                  fontSizeForHintText: 15.sp,
                  onChange: (value) {
                    CurrentRepository.totalArea = value;
                  },
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: InputTextField(
                  initValue: CurrentRepository.pricePerMeter == null
                      ? ""
                      : CurrentRepository.pricePerMeter.toString(),
                  hintTextColor: Colors.grey.shade900,
                  hintText: "price per meter (SAR)",
                  fontSizeForHintText: 15.sp,
                  onChange: (value) {
                    try {
                      double price = double.parse(value);
                      CurrentRepository.pricePerMeter = price;
                    } catch (e) {
                      displayDialog(context, "wrong Input (^_^)",
                          "press insert a number");
                    }
                  },
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: InputTextField(
                  initValue: CurrentRepository.streetWidth == null
                      ? ""
                      : CurrentRepository.streetWidth.toString(),
                  hintTextColor: Colors.grey.shade900,
                  hintText: "Street width (m)",
                  fontSizeForHintText: 15.sp,
                  onChange: (value) {
                    try {
                      double v = double.parse(value);
                      CurrentRepository.streetWidth = v;
                    } catch (e) {
                      displayDialog(context, "wrong Input (^_^)",
                          "press insert a number");
                    }
                  },
                ),
              ),
            ],
          ),
          TableRow(
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Builder(builder: (context) {
                  String imageName = 'Images';
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.h),
                        child: Text(
                          imageName,
                          style:
                              TextStyle(fontSize: 15.sp, color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 15.h),
                        child: GestureDetector(
                            onTap: () async {
                              XFile? image = await showDialog(
                                builder: (context) => const ShowImageDialog(),
                                context: context,
                              );
                              showDialog(
                                builder: (context) => AlertDialog(
                                  content: FutureBuilder(
                                    future: CloudStorageRepository.instance
                                        .uploadFile(
                                            filePath: image!.path,
                                            fileName: image.name),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<String> snapshot) {
                                      if (snapshot.hasData) {
                                        CurrentRepository.imageURLs!
                                            .add(snapshot.data!);
                                        print(snapshot.data!);
                                        return SizedBox(
                                          height: getScreenHeight(context) / 10,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Text("Uploaded",
                                                  style: TextStyle(
                                                      fontSize: 25.sp,
                                                      color: Colors.green)),
                                              const Center(
                                                child: Text("Done"),
                                              ),
                                            ],
                                          ),
                                        );
                                      } else {
                                        return SizedBox(
                                          height: getScreenHeight(context) / 10,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: const [
                                              Text("Uploading"),
                                              Center(
                                                child:
                                                    LinearProgressIndicator(),
                                              ),
                                            ],
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                ),
                                context: context,
                              );
                            },
                            child: Icon(Icons.add, size: 25.w)),
                      ),
                    ],
                  );
                }),
              ),
            ],
          ),
          TableRow(
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: InputTextField(
                  initValue: CurrentRepository.description ?? "",
                  hintTextColor: Colors.grey.shade900,
                  hintText: "Description",
                  fontSizeForHintText: 15.sp,
                  onChange: (value) {
                    CurrentRepository.description = value;
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
