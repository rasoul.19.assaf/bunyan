import 'package:bunyan/presentation/screens/AddingPropertyScreen/adding_property_step_one_screen.dart';
import 'package:bunyan/presentation/widgets/my_app_bar_secondary.dart';
import 'package:bunyan/presentation/widgets/my_button_primary.dart';
import 'package:bunyan/presentation/widgets/my_info_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PropertiesListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBarSecondary.getAppBar(
        backButton: (){
          Navigator.pop(context);
        },
        title: "Account",
        icon: IconButton(
          icon: const Icon(
            Icons.settings,
            color: Colors.black,
          ),
          tooltip: 'Open shopping cart',
          onPressed: () {
            // handle the press
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    "Properties List",
                    style: TextStyle(
                      fontSize: 18.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: MyInfoList(),
                  ),
                  SizedBox(height: 20.h),
                ],
              ),
            ),
            Center(
              child: SizedBox(
                width: double.infinity,
                child: MyButtonPrimary(
                  text: 'Add Property',
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            AddingPropertyStepOneScreen(),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
          ],
        ),
      ),
    );
  }
}
