import 'package:bunyan/data/repositories/firestore_repository.dart';
import '../../models/property/Commercial_property.dart';
import '../../models/property/Residential_property.dart';
import '../../models/property/VacantLand_property.dart';
import '../../models/property/property.dart';

class CurrentRepository {
  static String? name;
  static String? id;
  static String? ownerId;
  static String? houseAge;
  static double? price;
  static String? title;
  static String? location;
  static String? totalArea;
  static String? description;
  static List<String>? imageURLs = [];
  static String? transactionDate;
  static String? distanceToMRT;
  static String? numOfStore;
  static String? city;
  static String? neighborhood;
  static String? latitude;
  static String? longitude;
  static String? nearestHospital;
  static String? nearestSchoolOrCollage;
  static String? nearestMarket;
  static String? nearestMosque;
  static String? cityCenter;
  static double? streetWidth;
  static double? pricePerMeter;
  static int? floorNumber;
  static int? age;
  static int? rooms;
  static int? bathRooms;
  static int? kitchen;
  static String? streetDirection;

  static late PropertyType? propertyType;

  static cleanStore() {
    propertyType = null;
    floorNumber = null;
    age = null;
    rooms = null;
    bathRooms = null;
    kitchen = null;
    streetDirection = null;
    name = null;
    id = null;
    ownerId = null;
    houseAge = null;
    price = null;
    title = null;
    location = null;
    totalArea = null;
    description = null;
    imageURLs = [];
    transactionDate = null;
    distanceToMRT = null;
    numOfStore = null;
    city = null;
    neighborhood = null;
    latitude = null;
    longitude = null;
    nearestHospital = null;
    nearestSchoolOrCollage = null;
    nearestMarket = null;
    nearestMosque = null;
    cityCenter = null;
    streetWidth = null;
    pricePerMeter = null;
  }

  static Property getProperty() {
    if (propertyType == PropertyType.vacantLand) {
      VacantLandProperty vacantLandProperty = VacantLandProperty(
        pricePerMeter: pricePerMeter,
        streetWidth: streetWidth,
        ownerId: ownerId,
        name: name.toString(),
        houseAge: houseAge,
        imageURLs: imageURLs,
        price: price,
        title: title,
        location: location,
        totalArea: totalArea,
        description: description,
        transactionDate: transactionDate,
        distanceToMRT: distanceToMRT,
        numOfStore: numOfStore,
        city: city,
        neighborhood: neighborhood,
        latitude: latitude,
        longitude: longitude,
        nearestHospital: nearestHospital,
        nearestSchoolOrCollage: nearestSchoolOrCollage,
        nearestMarket: nearestMarket,
        nearestMosque: nearestMosque,
        cityCenter: cityCenter,
      );
      return vacantLandProperty;
    } else if (propertyType == PropertyType.commercial) {
      CommercialProperty commercialProperty = CommercialProperty(
        age: age,
        streetWidth: streetWidth,
        ownerId: ownerId,
        streetDirection: streetDirection,
        name: name.toString(),
        imageURLs: imageURLs,
        houseAge: houseAge,
        price: price,
        title: title,
        location: location,
        totalArea: totalArea,
        description: description,
        transactionDate: transactionDate,
        distanceToMRT: distanceToMRT,
        numOfStore: numOfStore,
        city: city,
        neighborhood: neighborhood,
        latitude: latitude,
        longitude: longitude,
        nearestHospital: nearestHospital,
        nearestSchoolOrCollage: nearestSchoolOrCollage,
        nearestMarket: nearestMarket,
        nearestMosque: nearestMosque,
        cityCenter: cityCenter,
      );
      return commercialProperty;
    } else {
      ResidentialProperty residentialProperty = ResidentialProperty(
        age: age,
        rooms: rooms,
        bathRooms: bathRooms,
        kitchen: kitchen,
        imageURLs: imageURLs,
        ownerId: ownerId,
        floorNumber: floorNumber,
        name: name.toString(),
        houseAge: houseAge,
        price: price,
        title: title,
        location: location,
        totalArea: totalArea,
        description: description,
        transactionDate: transactionDate,
        distanceToMRT: distanceToMRT,
        numOfStore: numOfStore,
        city: city,
        neighborhood: neighborhood,
        latitude: latitude,
        longitude: longitude,
        nearestHospital: nearestHospital,
        nearestSchoolOrCollage: nearestSchoolOrCollage,
        nearestMarket: nearestMarket,
        nearestMosque: nearestMosque,
        cityCenter: cityCenter,
      );
      return residentialProperty;
    }
  }
}
