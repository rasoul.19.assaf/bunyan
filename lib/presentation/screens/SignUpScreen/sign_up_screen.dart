import 'package:bunyan/data/repositories/authentication_repository.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/presentation/screens/HomeScreen/home_screen.dart';
import 'package:bunyan/presentation/widgets/my_button_secondary.dart';
import 'package:bunyan/presentation/widgets/my_text_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../models/app_user/app_user.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool isBusinessUser = false;
  String email = '';
  String username = '';
  String password = '';
  String passwordConfirm = '';
  String phoneNumber = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            opacity: 0.7,
            image: AssetImage("assets/images/register_background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 50.h,
                ),
                Text(
                  "Create Account",
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 38.sp,
                    color: Colors.white,
                    fontFamily: "AbrilFatface",
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.w),
                  width: double.infinity,
                  height: 100.h,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 12.w, vertical: 8.h),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isBusinessUser = false;
                              });
                            },
                            child: MyCard(
                              isSelected: !isBusinessUser,
                              title: 'Individual User',
                              icon: Icons.person,
                            ),
                          ),
                        ),
                        Expanded(
                            child: GestureDetector(
                          onTap: () {
                            setState(() {
                              isBusinessUser = true;
                            });
                          },
                          child: MyCard(
                            isSelected: isBusinessUser,
                            title: 'Business User',
                            icon: Icons.person_add,
                          ),
                        )),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      SizedBox(
                        child: MyTextField(
                          hintText: "Username",
                          onChanged: (value) {
                            username = value;
                          },
                        ),
                        height: 45.h,
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      SizedBox(
                        child: MyTextField(
                            hintText: "Email",
                            onChanged: (value) {
                              email = value;
                            }),
                        height: 45.h,
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      SizedBox(
                        child: MyTextField(
                            hintText: "Phone Number",
                            onChanged: (value) {
                              phoneNumber = value;
                            }),
                        height: 45.h,
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      SizedBox(
                        child: MyTextField(
                            hintText: "Password",
                            onChanged: (value) {
                              password = value;
                            }),
                        height: 45.h,
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      SizedBox(
                        child: MyTextField(
                            hintText: "Confirm Password",
                            onChanged: (value) {
                              passwordConfirm = value;
                            }),
                        height: 45.h,
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      SizedBox(
                        height: 45.h,
                        child: MyButtonSecondry(
                            text: 'Sign Up',
                            onPressed: () async {
                              if (!checkInputs()) {
                                return;
                              }
                              dynamic result = await AuthenticationRepository
                                  .instance
                                  .signUp(
                                      username, email, password, phoneNumber);
                              if (result != null) {
                                print(result);
                                NormalUser currentUser = NormalUser(
                                    type: isBusinessUser
                                        ? UserType.business
                                        : UserType.individual,
                                    id: FirebaseAuth.instance.currentUser!.uid,
                                    userName: username,
                                    email: email,
                                    phoneNumber: phoneNumber,
                                    chats: [],
                                    favoritesIds: [],
                                    ownedPropertiesIds: []);
                                FirestoreRepository.instance
                                    .createUserDoc(currentUser);
                                Navigator.of(context).pop();
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomeScreen()));
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return Material(
                                        child: Center(
                                          child: Text("error"),
                                        ),
                                      );
                                    });
                              }
                            }),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool checkInputs() {
    if (email.isEmpty) {
      print("email is empty");
    } else if (username.isEmpty) {
      print("username is empty");
    } else if (password.isEmpty) {
      print("password is empty");
    } else if (password != passwordConfirm) {
      print("passwords don't match");
    }
    return true;
  }
}

class MyCard extends StatelessWidget {
  final bool isSelected;
  final String title;
  final IconData? icon;

  const MyCard({Key? key, this.isSelected = false, this.title = "", this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: isSelected ? Colors.white : Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: isSelected ? 2 : 0,
      child: Container(
        color: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(title),
            Icon(
              icon,
              size: 28.sp,
            ),
          ],
        ),
      ),
    );
  }
}
