import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/app_user/app_user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/constants/constants.dart';
import '../../widgets/my_app_bar_secondary.dart';
import '../../widgets/my_button_primary.dart';
import 'chatting_screen.dart';

class ChatWithOwner extends StatelessWidget {
  ChatWithOwner({Key? key, this.ownerId}) : super(key: key);

  final String? ownerId;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: MyAppBarSecondary.getAppBar(
          title: "",
          backButton: () {
            Navigator.pop(context);
          },
        ),
        body: FutureBuilder(
          future: FirestoreRepository.instance
              .getOtherUserInfo(id: ownerId ?? 'Xc9jr5DuPCNqESRhNFDBruUgsvI3'),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (!snapshot.hasData) {
              print("Getting $ownerId document");
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              print('error');
              return Center(
                  child: CircularProgressIndicator(
                value: 1,
              ));
            } else {
              NormalUser owner = snapshot.data;
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 60.h),
                      Center(
                          child: Text(
                        owner.userName ?? 'name',
                        style: TextStyle(fontSize: 18.sp),
                      )),
                      SizedBox(height: 20.h),
                      Center(
                          child: Text(
                        owner.id ?? '??',
                        style: TextStyle(fontSize: 16.sp),
                      )),
                      SizedBox(height: 20.h),
                      Center(
                        child: SizedBox(
                          width: getScreenWidth(context) / 2,
                          child: MyButtonPrimary(
                              text: 'Chat with Owner',
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ChattingScreen(
                                      otherUserId: ownerId ??
                                          'SgQZpGN1wPML9izogxkxg7Nvd093',
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),
                      SizedBox(height: 40.h),
                      Text(
                        "Posted Ads",
                        style: TextStyle(
                            fontSize: 23.sp, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10.h),
                      Table(
                        border: const TableBorder(
                          bottom: BorderSide(color: Colors.grey, width: 2),
                          horizontalInside:
                              BorderSide(color: Colors.grey, width: 2),
                        ),
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.baseline,
                        defaultColumnWidth: const IntrinsicColumnWidth(),
                        columnWidths: const <int, TableColumnWidth>{
                          0: FlexColumnWidth(0.4),
                          1: FlexColumnWidth(0.6),
                          // 2: const MaxColumnWidth(FlexColumnWidth(2), FractionColumnWidth(0.3)),
                          // 3: const FixedColumnWidth(150),
                        },
                        textBaseline: TextBaseline.alphabetic,
                        children: [
                          TableRow(children: [
                            TableCell(
                              verticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 15.h),
                                child: Text(
                                  'Alraqdi Apartment',
                                  style: TextStyle(fontSize: 18.sp),
                                ),
                              ),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                              verticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 15.h),
                                child: Text(
                                  'Alraqdi Land',
                                  style: TextStyle(fontSize: 18.sp),
                                ),
                              ),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                              verticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 15.h),
                                child: Text(
                                  'xxxxx Building',
                                  style: TextStyle(fontSize: 18.sp),
                                ),
                              ),
                            ),
                          ]),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
