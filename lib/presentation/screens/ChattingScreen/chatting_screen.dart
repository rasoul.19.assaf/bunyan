import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/presentation/screens/HomePage/full_screen_image.dart';
import 'package:bunyan/presentation/widgets/my_app_bar_secondary.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

import '../../widgets/image_dialog.dart';

class ChattingScreen extends StatefulWidget {
  late String otherUserId;

  ChattingScreen({Key? key, required this.otherUserId}) : super(key: key);

  @override
  State<ChattingScreen> createState() => _ChattingScreenState();
}

class _ChattingScreenState extends State<ChattingScreen> {
  String? message;

  final TextEditingController _textFieldController = TextEditingController();

  ScrollController _scrollController = ScrollController(
    initialScrollOffset: 1000,
  );

  bool ifImage = false;

  XFile? image;

  bool imageUploading = false;

  @override
  void initState() {
    super.initState();
  }

  void getMessages() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBarSecondary.getAppBar(
        backButton: () {
          Navigator.pop(context);
        },
        title: '',
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                stream: FirebaseFirestore.instance
                    .collection('Messages')
                    .where(
                      "both",
                      arrayContains: FirebaseAuth.instance.currentUser!.uid,
                    )
                    .orderBy('createdAt')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  final messages = snapshot.data!.docs.where((element) {
                    List<dynamic> both = element.data()["both"];
                    return both.contains(widget.otherUserId);
                  });
                  if (messages.isEmpty) {
                    FirestoreRepository.instance.setNewChat(widget.otherUserId);
                  }
                  List<MessageBubble> messageBubbles = [];
                  for (var message in messages) {
                    print(message.data());
                    final messageText = message.data()['text'] ?? 'textNull';
                    final sender = message.data()['senderId'] ?? 'senderNull';
                    final currentUser = FirebaseAuth.instance.currentUser!.uid;
                    bool isMe = false;
                    if (currentUser == sender) {
                      isMe = true;
                    }
                    final messageBubble;
                    final imageUrl = message.data()['imageUrl'];
                    messageBubble = MessageBubble(
                        imageUrl: imageUrl == '' ? null : imageUrl,
                        sender: isMe ? null : message.data()['sender'],
                        text: messageText,
                        isMe: isMe);

                    messageBubbles.add(messageBubble);
                  }
                  return ListView(
                    controller: _scrollController,
                    children: messageBubbles,
                  );
                }),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 10.h),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: const Color(0xE08ebcd2),
              ),
              child: ListTile(
                leading: Transform.rotate(
                  angle: 45,
                  child: GestureDetector(
                      onTap: () async {
                        image = await showDialog(
                          builder: (context) => const ShowImageDialog(),
                          context: context,
                        );
                        if (image != null) {
                          ifImage = true;
                          String? cloudStoragePath;
                          setState(() {
                            imageUploading = true;
                          });
                          cloudStoragePath = await FirestoreRepository.instance
                              .sendImage(
                                  filePath: image!.path, fileName: image!.name);
                          setState(() {
                            imageUploading = true;
                          });
                          if (cloudStoragePath != null) {
                            FirestoreRepository.instance.sendMessage(
                                message: _textFieldController.text,
                                receiverId: widget.otherUserId,
                                receiverName: 'receiver',
                                imageUrl: cloudStoragePath);
                          }
                        }
                      },
                      child: const Icon(Icons.attach_file)),
                ),
                title: Expanded(
                  child: TextField(
                    controller: _textFieldController,
                    decoration: const InputDecoration.collapsed(
                        hintText: 'send a message'),
                  ),
                ),
                trailing: GestureDetector(
                  onTap: () async {
                    if (_textFieldController.text.isNotEmpty) {
                      FirestoreRepository.instance.sendMessage(
                          message: _textFieldController.text,
                          receiverId: widget.otherUserId,
                          receiverName: 'receiver',
                          imageUrl: '');
                      _textFieldController.clear();
                    }
                  },
                  child: Transform.rotate(
                    angle: -45,
                    child: const Icon(Icons.send_rounded),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String text;
  final String? sender;
  final bool isMe;

  final String? imageUrl;

  MessageBubble(
      {this.imageUrl,
      required this.sender,
      required this.text,
      required this.isMe});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          sender != null
              ? Text(
                  ' from  $sender',
                  style: TextStyle(color: Colors.black54, fontSize: 12.sp),
                )
              : const SizedBox(),
          Material(
            borderRadius: isMe
                ? const BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                    bottomRight: Radius.circular(30.0),
                  )
                : const BorderRadius.only(
                    topRight: Radius.circular(30.0),
                    bottomLeft: Radius.circular(30.0),
                    bottomRight: Radius.circular(30.0),
                  ),
            elevation: 5.0,
            color: isMe ? const Color(0xFF8ebcd2) : Colors.white,
            child: imageUrl != null
                ? SizedBox(
                    height: 200,
                    width: 250,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => FullScreenImage(
                              image: Image.network(
                                imageUrl!,
                                // 'assets/images/202-2nd-ave-hanover-pa-building-photo.jpg'
                              ),
                            ),
                          ),
                        );
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 5.h, horizontal: 5.w),
                        child: ClipRRect(
                          borderRadius: isMe
                              ? const BorderRadius.only(
                                  topLeft: Radius.circular(30.0),
                                  bottomLeft: Radius.circular(30.0),
                                  bottomRight: Radius.circular(30.0),
                                )
                              : const BorderRadius.only(
                                  topRight: Radius.circular(30.0),
                                  bottomLeft: Radius.circular(30.0),
                                  bottomRight: Radius.circular(30.0),
                                ),
                          child: Image.network(
                            imageUrl!,
                            // 'assets/images/202-2nd-ave-hanover-pa-building-photo.jpg'
                          ),
                        ),
                      ),
                    ),
                  )
                : Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
                    child: Text(
                      text,
                      style: TextStyle(
                        color: isMe ? Colors.white : Colors.black54,
                        fontSize: 15.sp,
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}

class MessegingTextField extends StatelessWidget {
  const MessegingTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ListTile();
  }
}
