import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/logic/bloc/commercial_properties_cubit.dart';
import 'package:bunyan/models/property/VacantLand_property.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../models/property/Commercial_property.dart';
import '../../models/property/Residential_property.dart';
import '../../models/property/property.dart';

class MyPropertyInfoTable extends StatelessWidget {
  double textSize = 15;
  final Property property;

  MyPropertyInfoTable({required this.property, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.w, right: 20.w),
      child: Table(
        border: const TableBorder(
          bottom: BorderSide(color: Colors.grey, width: 2),
          horizontalInside: BorderSide(color: Colors.grey, width: 2),
        ),
        defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
        defaultColumnWidth: const IntrinsicColumnWidth(),
        columnWidths: const <int, TableColumnWidth>{
          0: FlexColumnWidth(0.4),
          1: FlexColumnWidth(0.6),
        },
        textBaseline: TextBaseline.alphabetic,
        children: getRows(property),
      ),
    );
  }

  List<TableRow> getRows(Property property) {
    List<TableRow> list = [];
    if (property.name.isNotEmpty) {
      list.add(getTableRow(name: "Name", value: property.name, textSize: textSize));
    }
    if (property.description != null) {
      list.add(getTableRow(name: "Description", value: property.description!, textSize: textSize));
    }
    if (property.houseAge != null) {
      list.add(getTableRow(name: "House age", value: property.houseAge!, textSize: textSize));
    }
    if (property.price != null) {
      list.add(getTableRow(name: "Price", value: property.price.toString(), textSize: textSize));
    }
    if (property.location != null) {
      list.add(getTableRow(name: "Location", value: property.location!, textSize: textSize));
    }
    if (property.totalArea != null) {
      list.add(getTableRow(name: "Total Area", value: property.totalArea!, textSize: textSize));
    }
    if (property.transactionDate != null) {
      list.add(getTableRow(name: "Transaction Date", value: property.transactionDate!, textSize: textSize));
    }
    if (property.distanceToMRT != null) {
      list.add(
          getTableRow(name: "distance To the nearest MRT station", value: property.distanceToMRT!, textSize: textSize));
    }
    if (property.numOfStore != null) {
      list.add(getTableRow(name: "Number of convenience stores", value: property.numOfStore!, textSize: textSize));
    }
    if (property.city != null) {
      list.add(getTableRow(name: "City", value: property.city!, textSize: textSize));
    }

    if (property.neighborhood != null) {
      list.add(getTableRow(name: "Neighborhood", value: property.neighborhood!, textSize: textSize));
    }
    if (property.latitude != null) {
      list.add(getTableRow(name: "Latitude", value: property.latitude!, textSize: textSize));
    }
    if (property.longitude != null) {
      list.add(getTableRow(name: "Longitude", value: property.longitude!, textSize: textSize));
    }

    if (property is CommercialProperty) {
      CommercialProperty commercialProperty = property;
      if (commercialProperty.age != null) {
        list.add(getTableRow(name: "Age", value: commercialProperty.age.toString(), textSize: textSize));
      }
      if (commercialProperty.streetWidth != null) {
        list.add(
            getTableRow(name: "Street Width", value: commercialProperty.streetWidth.toString(), textSize: textSize));
      }
      if (commercialProperty.streetDirection != null) {
        list.add(getTableRow(name: "Street Direction", value: commercialProperty.streetDirection!, textSize: textSize));
      }
    } else if (property is VacantLandProperty) {
      VacantLandProperty vacantLandProperty = property;
      if (vacantLandProperty.streetWidth != null) {
        list.add(
            getTableRow(name: "Street Width", value: vacantLandProperty.streetWidth.toString(), textSize: textSize));
      }
      if (vacantLandProperty.pricePerMeter != null) {
        list.add(getTableRow(
            name: "Price Per Meter", value: vacantLandProperty.pricePerMeter.toString(), textSize: textSize));
      }
    } else if (property is ResidentialProperty) {
      ResidentialProperty residentialProperty = property;
      if (residentialProperty.floorNumber != null) {
        list.add(
            getTableRow(name: "Floor Number", value: residentialProperty.floorNumber.toString(), textSize: textSize));
      }
      if (residentialProperty.age != null) {
        list.add(getTableRow(name: "Age", value: residentialProperty.age.toString(), textSize: textSize));
      }
      if (residentialProperty.rooms != null) {
        list.add(getTableRow(name: "Rooms", value: residentialProperty.rooms.toString(), textSize: textSize));
      }
      if (residentialProperty.bathRooms != null) {
        list.add(getTableRow(name: "BathRooms", value: residentialProperty.bathRooms.toString(), textSize: textSize));
      }
      if (residentialProperty.kitchen != null) {
        list.add(getTableRow(name: "Kitchen", value: residentialProperty.kitchen.toString(), textSize: textSize));
      }
    } else {
      return [];
    }
    return list;
  }
}

getTableRow({required String name, required String value, required double textSize}) {
  return TableRow(children: [
    TableCell(
      verticalAlignment: TableCellVerticalAlignment.middle,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 15.h),
        child: Text(
          name,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: textSize.sp),
        ),
      ),
    ),
    TableCell(
      verticalAlignment: TableCellVerticalAlignment.middle,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.h),
          child: Text(
            value,
            style: TextStyle(fontSize: textSize.sp),
          ),
        ),
      ),
    ),
  ]);
}
