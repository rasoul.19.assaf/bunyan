import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/app_user/app_user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyInfoTable extends StatelessWidget {
  const MyInfoTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NormalUser currentUser = FirestoreRepository.currentUser!;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "User Information",
          style: TextStyle(
            fontSize: 18.sp,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Table(
            border: const TableBorder(
              horizontalInside: BorderSide(color: Colors.grey, width: 2),
            ),
            defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
            defaultColumnWidth: const IntrinsicColumnWidth(),
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(0.4),
              1: FlexColumnWidth(0.6),
            },
            textBaseline: TextBaseline.alphabetic,
            children: [
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'Username',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Text(
                      currentUser.userName ?? 'username',
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'ID',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Text(
                      currentUser.id ?? "XX",
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'Email',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Text(
                      currentUser.email,
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'Phone Number',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Text(
                      currentUser.phoneNumber,
                      style: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ),
              ]),
            ],
          ),
        ),
        SizedBox(height: 30.h),
        Text(
          "Services",
          style: TextStyle(
            fontSize: 18.sp,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Table(
            border: const TableBorder(
              horizontalInside: BorderSide(color: Colors.grey, width: 2),
            ),
            defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
            defaultColumnWidth: const IntrinsicColumnWidth(),
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(0.4),
            },
            textBaseline: TextBaseline.alphabetic,
            children: [
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'Chats',
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      height: 30.h,
                      width: 50.w,
                      child: Center(
                        child: Text(
                          FirestoreRepository.instance.getChatsNumber(),
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: const Text(
                      'Favorites',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.blueGrey),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      height: 30.h,
                      width: 50.w,
                      child: Center(
                          child: Text(FirestoreRepository.instance
                              .getFavoritesCount())),
                    ),
                  ),
                ),
              ]),
            ],
          ),
        ),
      ],
    );
  }
}
