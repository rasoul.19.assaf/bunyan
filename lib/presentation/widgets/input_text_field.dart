import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InputTextField extends StatelessWidget {
  String? hintText;
  double? fontSizeForHintText;
  Function(String)? onChange;
  String? initValue;
  Color hintTextColor;

  InputTextField(
      {required this.hintText,
      required this.hintTextColor,
      this.initValue,
      required this.fontSizeForHintText,
      required this.onChange,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initValue,
      decoration: InputDecoration(
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: TextStyle(color: hintTextColor, fontSize: fontSizeForHintText),
        border: InputBorder.none,
      ),
      onChanged: onChange,
    );
  }
}
