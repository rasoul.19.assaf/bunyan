import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyStepper extends StatelessWidget {
  MyStepper({required this.stepperIndex, required this.onStepperPressed, required this.stepperWidth, Key? key})
      : super(key: key);

  Function(int) onStepperPressed;
  int stepperIndex;
  double stepperWidth;

  @override
  Widget build(BuildContext context) {
    Color? activeColor = Colors.blue[200];
    Color? unActive = Colors.white;
    return Container(
      padding:  EdgeInsets.only(top: 20.h, bottom: 20.h),
      width: stepperWidth,
      child: Row(
        children: <Widget>[
          Container(
            width: 40.w,
            height: 40.w,
            decoration: BoxDecoration(
              color: stepperIndex == 0 ? activeColor : unActive,
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(30)),
            ),
            child: Center(
              child: TextButton(
                onPressed: () {
                  onStepperPressed(0);
                },
                child: Text(
                  "1",
                  style: TextStyle(fontSize: 18.sp, color: Colors.black),
                ),
              ),
            ),
          ),
          const Expanded(
            child: Divider(
              color: Colors.black,
              thickness: 1.5,
            ),
          ),
          Container(
            width: 40.w,
            height: 40.w,
            decoration: BoxDecoration(
              color: stepperIndex == 1 ? activeColor : unActive,
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(30)),
            ),
            child: Center(
              child: TextButton(
                onPressed: () {
                  onStepperPressed(1);
                },
                child: Text(
                  "2",
                  style: TextStyle(fontSize: 18.sp, color: Colors.black),
                ),
              ),
            ),
          ),
          const Expanded(
            child: Divider(
              color: Colors.black,
              thickness: 1.5,
            ),
          ),
          Container(
            width: 40.w,
            height: 40.w,
            decoration: BoxDecoration(
              color: stepperIndex == 2 ? activeColor : unActive,
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(30)),
            ),
            child: Center(
              child: TextButton(
                onPressed: () {
                  onStepperPressed(2);
                },
                child: Text(
                  "3",
                  style: TextStyle(fontSize: 18.sp, color: Colors.black),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
