import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:bunyan/presentation/screens/HomePage/property_information.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyImageCard extends StatefulWidget {
  Property property;
  final String title;
  final String price;
  bool isFavorite;

  MyImageCard(
      {Key? key,
      required this.property,
      required this.title,
      required this.price,
      this.isFavorite = false})
      : super(key: key);

  @override
  State<MyImageCard> createState() => _MyImageCardState();
}

class _MyImageCardState extends State<MyImageCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => PropertyInformation(
                property: widget.property,
              ),
            ),
          );
        },
        child: SizedBox(
          width: double.infinity,
          height: 200.h,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blueGrey,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: widget.property.imageURLs?[0] != null
                            ? NetworkImage(widget.property.imageURLs![0])
                            : const NetworkImage(
                                'https://juliaja.nl/wp-content/uploads/2021/04/noimagefound24-1024x576.png'),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                ),
              ),
              Container(
                width: double.infinity,
                height: 40.h,
                decoration: const BoxDecoration(
                    color: Color(0xFF8ebcd2),
                    borderRadius: const BorderRadius.all(Radius.circular(5))),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 12.w, vertical: 8.h),
                  child: Row(
                    children: [
                      Text(widget.property.title ?? ""),
                      Text(widget.property.price.toString() + " SAR"),
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                ),
              ),
              Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () async {
                      setState(() {
                        widget.isFavorite = !widget.isFavorite;
                      });
                      await FirestoreRepository.instance
                          .setFavorite(widget.property, widget.isFavorite);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 15.w, top: 10.h),
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Icon(
                        widget.isFavorite
                            ? Icons.favorite
                            : Icons.favorite_border_rounded,
                        color: Colors.red,
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
