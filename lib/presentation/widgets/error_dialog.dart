import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void displayDialog(context, title, text) => showDialog(
  context: context,
  builder: (context) => AlertDialog(
    title: Center(
      child: Text(title, style: TextStyle(color: Colors.red, fontSize: 30.sp, fontWeight: FontWeight.bold)),
    ),
    content: Text(text,
        style: TextStyle(
          color: Colors.red,
          fontSize: 25.sp,
        )),
    backgroundColor: Colors.black54,
    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
  ),
);