import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyTextField extends StatelessWidget {
  final hintText;
  final isObscure;
  void Function(String)? onChanged;

  MyTextField({Key? key, this.hintText, this.isObscure = false, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged ?? (v) {},
      obscureText: isObscure,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderSide: BorderSide.none),
        fillColor: Colors.white,
        isDense: false,
        filled: true,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.w),
        hintStyle: TextStyle(color: Colors.grey),
        hintText: hintText,
      ),
    );
  }
}
