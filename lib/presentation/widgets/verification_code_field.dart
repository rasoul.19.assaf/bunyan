import 'package:bunyan/core/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationCodeField extends StatefulWidget {
  VerificationCodeField(
      {Key? key,
      required this.textEditingController,
      required this.hasError,
      required this.length})
      : super(key: key);

  TextEditingController textEditingController;
  bool hasError;
  int length;

  @override
  _VerificationCodeFieldState createState() => _VerificationCodeFieldState();
}

class _VerificationCodeFieldState extends State<VerificationCodeField> {
  String currentText = '';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: getScreenWidth(context) - 100.w,
        child: PinCodeTextField(
          appContext: context,
          length: widget.length,
          obscureText: false,
          animationType: AnimationType.scale,
          pinTheme: PinTheme(
            shape: PinCodeFieldShape.box,
            borderRadius: BorderRadius.circular(5),
            fieldHeight: 55.h,
            fieldWidth: 60.w,
            activeColor: Colors.white,
            inactiveColor: Colors.white,
            selectedColor: Colors.lightBlueAccent,
            activeFillColor: Colors.white,
            selectedFillColor: Colors.white,
            inactiveFillColor: Colors.white,
          ),
          animationDuration: const Duration(milliseconds: 300),
          backgroundColor: const Color(0x008ebcd2),
          enableActiveFill: true,
          controller: widget.textEditingController,
          keyboardType: TextInputType.number,
          onCompleted: (v) {
            if (v.length != 4) {
              setState(() {
                widget.hasError = true;
              });
            } else {
              setState(() {
                widget.hasError = false;
              });
            }
            widget.textEditingController.text = v;
          },
          onChanged: (value) {
            setState(() {
              currentText = value;
            });
          },
        ),
      ),
    );
  }
}
