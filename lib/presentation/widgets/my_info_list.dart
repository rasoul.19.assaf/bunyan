import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyInfoList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getProperties(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (snapshot.hasError) {
          return Container(
            height: 100,
            width: 50,
            color: Colors.red,
          );
        } else {
          List<TableRow> properties = snapshot.data as List<TableRow>;
          properties.addAll(dummyData);
          return Table(
            border: const TableBorder(
              horizontalInside: BorderSide(color: Colors.grey, width: 2),
            ),
            defaultVerticalAlignment: TableCellVerticalAlignment.baseline,
            defaultColumnWidth: const IntrinsicColumnWidth(),
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(0.4),
              1: FlexColumnWidth(0.6),
            },
            textBaseline: TextBaseline.alphabetic,
            children: properties,
          );
        }
      },
    );
  }

  Future<List<TableRow>> getProperties() async {
    List<TableRow> rows = [];
    List<Property> ownedProperties =
        await FirestoreRepository.instance.getOwnedProperties();
    print("Owned Properties");
    print(ownedProperties.length);
    for (var property in ownedProperties) {
      rows.add(
        TableRow(children: [
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.middle,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20.h),
              child: Text(
                property.name,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black54),
              ),
            ),
          ),
        ]),
      );
    }

    return rows;
  }

  List<TableRow> dummyData = [
    TableRow(children: [
      TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: const Text(
            'xx Land',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),
          ),
        ),
      ),
    ]),
    TableRow(children: [
      TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: const Text(
            'xxx Building',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),
          ),
        ),
      ),
    ]),
    TableRow(children: [
      TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: const Text(
            'xxxx Store',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),
          ),
        ),
      ),
    ]),
    TableRow(children: [
      TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.h),
          child: const Text(
            'xxxxx Villa',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),
          ),
        ),
      ),
    ]),
  ];
}
