import 'package:bunyan/presentation/screens/HomePage/full_screen_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../core/constants/constants.dart';

class AlbumSlider extends StatefulWidget {
  AlbumSlider(
      {Key? key,
      required this.heightOfController,
      required this.height,
      required this.itemCount,
      required this.items})
      : super(key: key);

  List<Widget> items;
  int itemCount;
  double height;
  double heightOfController;

  @override
  _AlbumSliderState createState() => _AlbumSliderState();
}

class _AlbumSliderState extends State<AlbumSlider> {
  CarouselController buttonCarouselController = CarouselController();
  int carouselIndex = 1;
  Color nextIconColor = Colors.grey;
  Color previousIconColor = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      width: getScreenWidth(context),
      child: Stack(
        alignment: Alignment.center,
        textDirection: TextDirection.rtl,
        fit: StackFit.loose,
        clipBehavior: Clip.hardEdge,
        children: <Widget>[
          CarouselSlider.builder(
            itemCount: widget.itemCount,
            itemBuilder: (context, index, realIndex) {
              return widget.items[index];
            },
            carouselController: buttonCarouselController,
            options: CarouselOptions(
              onPageChanged: (index, reason) {
                setState(() {
                  carouselIndex = index + 1;
                });
              },
              height: widget.height,
              viewportFraction: 1,
              autoPlay: false,
              enableInfiniteScroll: false,
              enlargeCenterPage: true,
              enlargeStrategy: CenterPageEnlargeStrategy.height,
            ),
          ),
          Positioned(
            top: widget.height - widget.heightOfController,
            width: MediaQuery.of(context).size.width,
            child: Container(
              height: widget.heightOfController,
              width: MediaQuery.of(context).size.width,
              color: Colors.white70.withOpacity(0.5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  const Spacer(flex: 3),
                  GestureDetector(
                    onTap: () {
                      if (carouselIndex > 1) {
                        setState(() {
                          previousIconColor = Colors.black;
                          nextIconColor = Colors.grey;
                          carouselIndex--;
                        });
                      }
                      buttonCarouselController.previousPage(
                          curve: Curves.linear);
                    },
                    child: Icon(
                      Icons.arrow_left_sharp,
                      color: previousIconColor,
                      size: 45.w,
                    ),
                  ),
                  Text(
                    widget.itemCount == 0
                        ? "None"
                        : "$carouselIndex  of  ${widget.itemCount}",
                    style: TextStyle(fontSize: 20.sp),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (carouselIndex < widget.itemCount) {
                        setState(() {
                          previousIconColor = Colors.grey;
                          nextIconColor = Colors.black;
                          carouselIndex++;
                        });
                      }
                      buttonCarouselController.nextPage(curve: Curves.linear);
                    },
                    child: Icon(
                      Icons.arrow_right_sharp,
                      color: nextIconColor,
                      size: 45.sp,
                    ),
                  ),
                  const Spacer(flex: 2),
                  IconButton(
                    splashColor: Colors.transparent,
                    color: Colors.black,
                    iconSize: 30.w,
                    icon: const Icon(Icons.open_in_full_sharp),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FullScreenImage(
                                image: widget.items[carouselIndex - 1])),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
