import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyAppBarSecondary {
  static AppBar getAppBar(
      {required VoidCallback backButton,
      required String title,
      Widget icon = const SizedBox()}) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: const TextStyle(color: Colors.black),
      ),
      elevation: 2,
      leadingWidth: 80.w,
      centerTitle: true,
      actions: [
        icon,
        SizedBox(width: 8.w),
      ],
      leading: GestureDetector(
        onTap: backButton,
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 5.w),
              child: const Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.grey,
              ),
            ),
            const Text(
              "Back",
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
