import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirestoreProvider {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  void createUserDocument(Map<String, dynamic> json) {
    json['userChats'] = [];
    firestore.collection('Users').doc(json['id']).set(json);
  }

  Future<DocumentSnapshot<Map<String, dynamic>>> getUserDocument(
      String uid) async {
    return await firestore.collection("Users").doc(uid).get();
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>> getProperties(
      PropertyType type) async {
    print("getting + ${type.name}");
    return await firestore
        .collection("Properties")
        .where('type', isEqualTo: type.name)
        .get()
        .then((value) => value.docs);
  }

  void sendMessage(Map<String, dynamic> message) {
    firestore.collection('Messages').add(message);
  }

  Future<void> setFavorite(String propertyId, bool isFavorite) async {
    await firestore
        .collection("Users")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
          "favoritesIds": isFavorite
              ? FieldValue.arrayUnion([propertyId])
              : FieldValue.arrayRemove([propertyId]),
        })
        .onError((error, stackTrace) => {print(error)})
        .then((value) => print('done'));
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>>
      getOwnedProperties() async {
    return await firestore
        .collection('Properties')
        .where('ownerId', isEqualTo: FirestoreRepository.currentUser!.id)
        // .limit(10)
        .get()
        .then((value) => value.docs);
  }

  void addProperty(Map<String, dynamic> json, PropertyType type) async {
    json['type'] = type.name;
    await firestore.collection('Properties').add(json);
  }

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>>
      getFavoriteProperties(String? id) async {
    return await firestore
        .collection("Properties")
        .where('likedBy', arrayContains: id)
        .get()
        .then((value) => value.docs);
  }

  void getMessagesDocs() {
    //DONE implement
  }

  Future<void> setPropertyLiked(String id, bool isFavorite) async {
    await firestore
        .collection("Properties")
        .doc(id)
        .update({
          "likedBy": isFavorite
              ? FieldValue.arrayUnion([FirebaseAuth.instance.currentUser!.uid])
              : FieldValue.arrayRemove(
                  [FirebaseAuth.instance.currentUser!.uid]),
        })
        .onError((error, stackTrace) => {print(error)})
        .then((value) => print('done $isFavorite'));
  }

  Future<bool> initChat(String otherUserId) async {
    try {
      await firestore
          .collection("Users")
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .update({
        "userChats": FieldValue.arrayUnion([otherUserId]),
      });
      return true;
    } catch (e) {
      return false;
    }
  }
}
