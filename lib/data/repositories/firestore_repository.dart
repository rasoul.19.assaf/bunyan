import 'package:bunyan/data/data_providers/firestore_provider.dart';
import 'package:bunyan/data/repositories/cloud_storage_repository.dart';
import 'package:bunyan/models/app_user/app_user.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

import '../../models/property/Commercial_property.dart';
import '../../models/property/Residential_property.dart';
import '../../models/property/VacantLand_property.dart';

class FirestoreRepository {
  static FirestoreRepository? _instance;
  static NormalUser? currentUser;
  List<String> userChats = [];
  final FirestoreProvider firestoreProvider = FirestoreProvider();

  // Map<String, Property> allProperties = {};
  List<CommercialProperty> commercialProperties = [];
  List<VacantLandProperty> vacantLandProperties = [];
  List<ResidentialProperty> residentialProperties = [];

  FirestoreRepository._privateConstructor();

  static FirestoreRepository get instance {
    _instance ??= FirestoreRepository._privateConstructor();
    return _instance!;
  }

  void createUserDoc(NormalUser user) {
    print("Creating user doc");
    currentUser = user;
    print(user.toJson());
    firestoreProvider.createUserDocument(user.toJson());
  }

  Future<void> initCurrentUser(auth.User user) async {
    DocumentSnapshot<Map<String, dynamic>> result =
        await firestoreProvider.getUserDocument(user.uid);
    print(result.data());
    userChats = result.data()!["userChats"] ?? [];
    currentUser = NormalUser.fromJson(result.data()!);
    currentUser!.copyWith(id: user.uid);
  }

  bool propertyIsFavorite(String id) {
    try {
      return currentUser!.favoritesIds!.contains(id);
    } catch (e) {
      return false;
    }
  }

/*  Future<Unit> fetchResidentialProperties() async {
    try {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> propertiesDocs =
          await firestoreProvider.getProperties(PropertyType.residential);
      residentialProperties
          .addAll(_getModelList(propertiesDocs, ResidentialProperty.fromJson));
    } catch (e) {
      throw Exception();
    }
    return unit;
  }*/

/*
  Future<Unit> fetchVacantLandProperties() async {
    try {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> propertiesDocs =
          await firestoreProvider.getProperties(PropertyType.vacantLand);
      vacantLandProperties
          .addAll(_getModelList(propertiesDocs, VacantLandProperty.fromJson));
    } catch (e) {
      throw Exception();
    }
    return unit;
  }
*/

  Future<bool> fetchResidentialProperties() async {
    List<QueryDocumentSnapshot<Map<String, dynamic>>> propertiesDocs =
        await firestoreProvider.getProperties(PropertyType.residential);
    print("<<    :_  property  _:      >>");
    // print(propertiesDocs[0].data());

    for (var element in propertiesDocs) {
      try {
        ResidentialProperty property =
            ResidentialProperty.fromJson(element.data());
        property.id = element.id;
        print(element.data());
        //DONE: add id to property object
        residentialProperties.add(property);
      } catch (e) {
        print('error fetching property with id: $element.id');
      }
      ////////////////////////////////////////////////
    }
    return true;
  }

  Future<bool> fetchVacantLandProperties() async {
    List<QueryDocumentSnapshot<Map<String, dynamic>>> propertiesDocs =
        await firestoreProvider.getProperties(PropertyType.vacantLand);
    print("<<    :_  property  _:      >>");

    for (var element in propertiesDocs) {
      try {
        VacantLandProperty property =
            VacantLandProperty.fromJson(element.data());
        property.id = element.id;
        print(element.data());
        //DONE: add id to property object
        vacantLandProperties.add(property);
      } catch (e) {
        print('error fetching property with id: $element.id');
      }
      ////////////////////////////////////////////////
    }
    return true;
  }

  Future<bool> fetchCommercialProperties() async {
    List<QueryDocumentSnapshot<Map<String, dynamic>>> propertiesDocs =
        await firestoreProvider.getProperties(PropertyType.commercial);
    print("<<    :_  property  _:      >>");
    // print(propertiesDocs[0].data());

    for (var element in propertiesDocs) {
      try {
        CommercialProperty property =
            CommercialProperty.fromJson(element.data());
        property.id = element.id;
        print(element.data());
        //DONE: add id to property object
        commercialProperties.add(property);
      } catch (e) {
        print('error fetching property with id: $element.id');
      }
      ////////////////////////////////////////////////

    }
    return true;
  }

  Future<List<CommercialProperty>> getCommercialProperties() async {
    print("getCommercialProperties");
    commercialProperties = [];
    if (commercialProperties.isEmpty) {
      await fetchCommercialProperties();
    }
    return commercialProperties;
  }

  Future<List<VacantLandProperty>> getVacantLandProperties() async {
    print("getVacantLandProperties");
    vacantLandProperties = [];
    if (vacantLandProperties.isEmpty) {
      await fetchVacantLandProperties();
    }
    return vacantLandProperties;
  }

  Future<List<ResidentialProperty>> getResidentialProperties() async {
    residentialProperties = [];
    if (residentialProperties.isEmpty) {
      await fetchResidentialProperties();
    }
    return residentialProperties;
  }

  Future<String> sendImage(
      {required String filePath, required String fileName}) async {
    String cloudStoragePath = await CloudStorageRepository.instance
        .uploadFile(filePath: filePath, fileName: fileName);
    return cloudStoragePath;
  }

  void sendMessage(
      {required String message,
      required String receiverId,
      required String receiverName,
      String? imageUrl}) {
    firestoreProvider.sendMessage({
      'sender': currentUser!.userName,
      'senderId': auth.FirebaseAuth.instance.currentUser?.uid,
      'receiver': receiverName,
      'receiverId': receiverId,
      'imageUrl': imageUrl ?? '',
      'both': [
        auth.FirebaseAuth.instance.currentUser?.uid,
        receiverId,
      ],
      'text': message,
      'createdAt': Timestamp.now(),
    });
  }

  void addProperty(Property property, PropertyType type) {
    print(property.toJson());
    firestoreProvider.addProperty(property.toJson(), type);
  }

  Future<NormalUser> getOtherUserInfo({required String id}) async {
    DocumentSnapshot<Map<String, dynamic>> result =
        await firestoreProvider.getUserDocument(id);
    NormalUser otherUser = NormalUser.fromJson(result.data()!);
    return otherUser;
  }

  Future setFavorite(Property property, bool isFavorite) async {
    firestoreProvider.setFavorite(property.id!, isFavorite);
    firestoreProvider.setPropertyLiked(property.id!, isFavorite);
    List<String>? favs = currentUser!.favoritesIds;
    favs ??= [];
    if (isFavorite) {
      favs.remove(property.id);
      currentUser!.copyWith(
        favoritesIds: favs,
      );
      property.removeLikedBy(property.id!);
    } else {
      favs.add(property.id!);
      currentUser!.copyWith(
        favoritesIds: favs,
      );
      property.addLikedBy(property.id!);
    }
    print(currentUser!.favoritesIds);
    print('info saved');
  }

  String getChatsNumber() {
    try {
      return "${userChats.length}";
    } catch (e) {
      return '0';
    }
  }

  String getFavoritesCount() {
    try {
      return "${currentUser?.favoritesIds?.length ?? 0}";
    } catch (e) {
      return '0';
    }
  }

  Future<List<Property>> getFavoritesProperties() async {
    List<QueryDocumentSnapshot<Map<String, dynamic>>> result =
        await firestoreProvider.getFavoriteProperties(currentUser!.id);
    List<Property> favoriteProperties = [];
    //DONE: Check this method
    for (var doc in result) {
      Property property = parseProperty(doc.data());
      property.id = doc.id;
      favoriteProperties.add(property);
    }

    return favoriteProperties;
  }

  Future<List<Property>> getOwnedProperties() async {
    List<QueryDocumentSnapshot<Map<String, dynamic>>> result =
        await firestoreProvider.getOwnedProperties();
    List<Property> ownedProperties = [];
    print('docs.length');
    print(result.length);
    for (var doc in result) {
      Property property = parseProperty(doc.data());
      property.id = doc.id;
      ownedProperties.add(property);
    }

    return ownedProperties;
  }

  Property parseProperty(Map<String, dynamic> data) {
    if (data['type'] == 'commercial') {
      return CommercialProperty.fromJson(data);
    } else if (data['type'] == 'residential') {
      return ResidentialProperty.fromJson(data);
    } else {
      return VacantLandProperty.fromJson(data);
    }
  }

  void saveUserInfo() {}

  Future<bool> setNewChat(String otherUserId) {
    userChats.add(otherUserId);
    return firestoreProvider.initChat(otherUserId);
  }
}

enum PropertyType {
  commercial,
  residential,
  vacantLand,
}

enum UserType {
  individual,
  business,
}
