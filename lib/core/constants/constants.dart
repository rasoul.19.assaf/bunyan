import 'package:flutter/material.dart';

List<Image> images = [
  Image.asset('assets/images/image1.jpg', fit: BoxFit.cover),
  Image.asset('assets/images/image2.jpg', fit: BoxFit.cover),
  Image.asset('assets/images/image3.jpg', fit: BoxFit.cover),
  Image.asset('assets/images/image4.jpg', fit: BoxFit.cover),
];

double getScreenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double getScreenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height -
      MediaQuery.of(context).padding.top -
      MediaQuery.of(context).padding.bottom;
}
