import 'package:bunyan/presentation/screens/current_repository.dart';
//
// List<double> weights = [
//   // -4.46984082e-03, No weight
//   4.99687347e+00,
//   -2.81817128e-01,
//   -4.32625415e-03,
//   1.10700052e+00,
//   2.61668493e+02,
//   -1.32295234e+01,
// ];

List<double> weights = [
  4.987668982312206,
  -0.2818922947542016,
  -0.004270185332336125,
  1.1198133683212756,
  263.956927516666,
  -11.589266041608592,
];

// double bias = -14941; with No
double bias = -15179.997770258622;

Future<double> calculatePrice() async {
  await Future.delayed(Duration(seconds: 3), () {});
  print("predicting price");
  double price = 0;
  // List<double> inputs = [2013.917, 17, 1083, 4, 24, 121];
  List<double> inputs = [2012.917, 32, 84.87882, 10, 24.98298, 121.54024];

  try {
    inputs[0] = double.parse(CurrentRepository.transactionDate!);
  } catch (e) {
    // inputs[0] = 2013;
  }
  try {
    inputs[1] = double.parse(CurrentRepository.houseAge!);
    print("age ${inputs[1]} ");
  } catch (e) {
    // inputs[1] = 17;
  }
  try {
    inputs[2] = double.parse(CurrentRepository.distanceToMRT!);
  } catch (e) {
    // inputs[2] = 1083;
  }
  try {
    inputs[3] = double.parse(CurrentRepository.numOfStore!);
  } catch (e) {
    // inputs[3] = 4;
  }
  try {
    inputs[4] = double.parse(CurrentRepository.latitude!);
  } catch (e) {
    // inputs[4] = 24;
  }
  try {
    inputs[5] = double.parse(CurrentRepository.longitude!);
  } catch (e) {
    // inputs[5] = 121;
  }

  for (int i = 0; i < 6; i++) {
    price += inputs[i] * weights[i];
  }
  price += bias;

  return price;
}
