import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tflite_flutter/tflite_flutter.dart' as tfl;

class TestModel extends StatefulWidget {
  const TestModel({Key? key}) : super(key: key);

  @override
  _TestModelState createState() => _TestModelState();
}

class _TestModelState extends State<TestModel> {
  executeModel(List<dynamic> inputs) async {
    final interpreter = await tfl.Interpreter.fromAsset('assets/AI_model/model.tflite');

    var input0 = [1.23];
    var input1 = [2.43];
    var inputs = [input0, input1, input0, input1];

    var output0 = List<double>.filled(1, 0);
    var output1 = List<double>.filled(1, 0);
    var outputs = {0: output0, 1: output1};

    interpreter.runForMultipleInputs(inputs, outputs);

    print(outputs);
    return outputs;
  }

  var result;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Container(
            height: 50.h,
            width: 100.w,
            color: Colors.teal,
            child: TextButton(
              onPressed: () {
                setState(() {
                  //result = executeModel();
                });
              },
              child: Text(
                "execute",
                style: TextStyle(fontSize: 20.sp),
              ),
            ),
          ),
          Text(
            result.toString(),
            style: TextStyle(fontSize: 20.sp),
          ),
        ],
      ),
    ));
  }
}
