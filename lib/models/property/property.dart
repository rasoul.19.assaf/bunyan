import 'package:json_annotation/json_annotation.dart';

part 'property.g.dart';

@JsonSerializable()
class Property {
  String name;
  String? id;
  String? ownerId;
  String? houseAge;
  double? price;
  String? title;
  String? location;
  String? totalArea;
  String? description;
  List<String>? likedBy;
  List<String>? imageURLs;
  String? transactionDate;
  String? distanceToMRT;
  String? numOfStore;
  String? city;
  String? neighborhood;
  String? latitude;
  String? longitude;
  String? nearestHospital;
  String? nearestSchoolOrCollage;
  String? nearestMarket;
  String? nearestMosque;
  String? cityCenter;

  Property(
      {this.id,
      this.houseAge,
      this.price,
      this.ownerId,
      this.title,
      this.location,
      this.totalArea,
      this.description,
      this.imageURLs,
      this.likedBy,
      this.transactionDate,
      this.distanceToMRT,
      this.numOfStore,
      this.city,
      this.neighborhood,
      this.latitude,
      this.longitude,
      this.nearestHospital,
      this.nearestSchoolOrCollage,
      this.nearestMarket,
      this.nearestMosque,
      this.cityCenter,
      required this.name});

  factory Property.fromJson(Map<String, dynamic> json) =>
      _$PropertyFromJson(json);

  Map<String, dynamic> toJson() => _$PropertyToJson(this);

  void removeLikedBy(String id) {
    likedBy ??= [];
    likedBy!.remove(id);
    print("remove liked By");
    print(likedBy);
  }

  void addLikedBy(String id) {
    likedBy ??= [];
    likedBy!.add(id);
    print("Add liked By");
    print(likedBy);
  }

  isLikedBy(String? id) {
    print("isLikedBy $id");
    likedBy ??= [];
    print(likedBy);
    return likedBy!.contains(id);
  }
}
