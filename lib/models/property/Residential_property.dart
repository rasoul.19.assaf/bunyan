import 'package:bunyan/models/property/property.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Residential_property.g.dart';

@JsonSerializable()
class ResidentialProperty extends Property {
  int? floorNumber;
  int? age;
  int? rooms;
  int? bathRooms;
  int? kitchen;

  ResidentialProperty({
    String? ownerId,
    String? id,
    String? houseAge,
    double? price,
    String? title,
    String? location,
    String? totalArea,
    String? description,
    List<String>? likedBy,
    List<String>? imageURLs,
    String? transactionDate,
    String? distanceToMRT,
    String? numOfStore,
    String? city,
    String? neighborhood,
    String? latitude,
    String? longitude,
    String? nearestHospital,
    String? nearestSchoolOrCollage,
    String? nearestMarket,
    String? nearestMosque,
    String? cityCenter,
    required String name,
    this.floorNumber,
    this.age,
    this.rooms,
    this.bathRooms,
    this.kitchen,
  }) : super(
          name: name,
          ownerId: ownerId,
          cityCenter: cityCenter,
          nearestMosque: nearestMosque,
          nearestMarket: nearestMarket,
          nearestSchoolOrCollage: nearestSchoolOrCollage,
          nearestHospital: nearestHospital,
          longitude: longitude,
          latitude: latitude,
          neighborhood: neighborhood,
          city: city,
          likedBy: likedBy,
          numOfStore: numOfStore,
          id: id,
          houseAge: houseAge,
          price: price,
          title: title,
          location: location,
          totalArea: totalArea,
          description: description,
    imageURLs: imageURLs,
          transactionDate: transactionDate,
          distanceToMRT: distanceToMRT,
        );

  @override
  factory ResidentialProperty.fromJson(Map<String, dynamic> json) => _$ResidentialPropertyFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ResidentialPropertyToJson(this);
}
