// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'VacantLand_property.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VacantLandProperty _$VacantLandPropertyFromJson(Map<String, dynamic> json) =>
    VacantLandProperty(
      ownerId: json['ownerId'] as String?,
      id: json['id'] as String?,
      houseAge: json['houseAge'] as String?,
      price: (json['price'] as num?)?.toDouble(),
      title: json['title'] as String?,
      location: json['location'] as String?,
      totalArea: json['totalArea'] as String?,
      description: json['description'] as String?,
      likedBy:
          (json['likedBy'] as List<dynamic>?)?.map((e) => e as String).toList(),
      imageURLs: (json['imageURLs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      transactionDate: json['transactionDate'] as String?,
      distanceToMRT: json['distanceToMRT'] as String?,
      numOfStore: json['numOfStore'] as String?,
      city: json['city'] as String?,
      neighborhood: json['neighborhood'] as String?,
      latitude: json['latitude'] as String?,
      longitude: json['longitude'] as String?,
      nearestHospital: json['nearestHospital'] as String?,
      nearestSchoolOrCollage: json['nearestSchoolOrCollage'] as String?,
      nearestMarket: json['nearestMarket'] as String?,
      nearestMosque: json['nearestMosque'] as String?,
      cityCenter: json['cityCenter'] as String?,
      name: json['name'] as String,
      streetWidth: (json['streetWidth'] as num?)?.toDouble(),
      pricePerMeter: (json['pricePerMeter'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$VacantLandPropertyToJson(VacantLandProperty instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'ownerId': instance.ownerId,
      'houseAge': instance.houseAge,
      'price': instance.price,
      'title': instance.title,
      'location': instance.location,
      'totalArea': instance.totalArea,
      'description': instance.description,
      'likedBy': instance.likedBy,
      'imageURLs': instance.imageURLs,
      'transactionDate': instance.transactionDate,
      'distanceToMRT': instance.distanceToMRT,
      'numOfStore': instance.numOfStore,
      'city': instance.city,
      'neighborhood': instance.neighborhood,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'nearestHospital': instance.nearestHospital,
      'nearestSchoolOrCollage': instance.nearestSchoolOrCollage,
      'nearestMarket': instance.nearestMarket,
      'nearestMosque': instance.nearestMosque,
      'cityCenter': instance.cityCenter,
      'streetWidth': instance.streetWidth,
      'pricePerMeter': instance.pricePerMeter,
    };
