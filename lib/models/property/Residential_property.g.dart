// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Residential_property.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResidentialProperty _$ResidentialPropertyFromJson(Map<String, dynamic> json) =>
    ResidentialProperty(
      ownerId: json['ownerId'] as String?,
      id: json['id'] as String?,
      houseAge: json['houseAge'] as String?,
      price: (json['price'] as num?)?.toDouble(),
      title: json['title'] as String?,
      location: json['location'] as String?,
      totalArea: json['totalArea'] as String?,
      description: json['description'] as String?,
      likedBy:
          (json['likedBy'] as List<dynamic>?)?.map((e) => e as String).toList(),
      imageURLs: (json['imageURLs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      transactionDate: json['transactionDate'] as String?,
      distanceToMRT: json['distanceToMRT'] as String?,
      numOfStore: json['numOfStore'] as String?,
      city: json['city'] as String?,
      neighborhood: json['neighborhood'] as String?,
      latitude: json['latitude'] as String?,
      longitude: json['longitude'] as String?,
      nearestHospital: json['nearestHospital'] as String?,
      nearestSchoolOrCollage: json['nearestSchoolOrCollage'] as String?,
      nearestMarket: json['nearestMarket'] as String?,
      nearestMosque: json['nearestMosque'] as String?,
      cityCenter: json['cityCenter'] as String?,
      name: json['name'] as String,
      floorNumber: json['floorNumber'] as int?,
      age: json['age'] as int?,
      rooms: json['rooms'] as int?,
      bathRooms: json['bathRooms'] as int?,
      kitchen: json['kitchen'] as int?,
    );

Map<String, dynamic> _$ResidentialPropertyToJson(
        ResidentialProperty instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'ownerId': instance.ownerId,
      'houseAge': instance.houseAge,
      'price': instance.price,
      'title': instance.title,
      'location': instance.location,
      'totalArea': instance.totalArea,
      'description': instance.description,
      'likedBy': instance.likedBy,
      'imageURLs': instance.imageURLs,
      'transactionDate': instance.transactionDate,
      'distanceToMRT': instance.distanceToMRT,
      'numOfStore': instance.numOfStore,
      'city': instance.city,
      'neighborhood': instance.neighborhood,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'nearestHospital': instance.nearestHospital,
      'nearestSchoolOrCollage': instance.nearestSchoolOrCollage,
      'nearestMarket': instance.nearestMarket,
      'nearestMosque': instance.nearestMosque,
      'cityCenter': instance.cityCenter,
      'floorNumber': instance.floorNumber,
      'age': instance.age,
      'rooms': instance.rooms,
      'bathRooms': instance.bathRooms,
      'kitchen': instance.kitchen,
    };
