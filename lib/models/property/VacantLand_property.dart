
import 'package:bunyan/models/property/property.dart';
import 'package:json_annotation/json_annotation.dart';

part 'VacantLand_property.g.dart';

@JsonSerializable()
class VacantLandProperty extends Property {
  double? streetWidth;
  double? pricePerMeter;

  VacantLandProperty({
    String? ownerId,
    String? id,
    String? houseAge,
    double? price,
    String? title,
    String? location,
    String? totalArea,
    String? description,
    List<String>? likedBy,
    List<String>? imageURLs,
    String? transactionDate,
    String? distanceToMRT,
    String? numOfStore,
    String? city,
    String? neighborhood,
    String? latitude,
    String? longitude,
    String? nearestHospital,
    String? nearestSchoolOrCollage,
    String? nearestMarket,
    String? nearestMosque,
    String? cityCenter,
    required String name,
    this.streetWidth,
    this.pricePerMeter,
  }) : super(
          name: name,
          ownerId: ownerId,
          likedBy: likedBy,
          cityCenter: cityCenter,
          nearestMosque: nearestMosque,
          nearestMarket: nearestMarket,
          nearestSchoolOrCollage: nearestSchoolOrCollage,
          nearestHospital: nearestHospital,
          longitude: longitude,
          latitude: latitude,
          neighborhood: neighborhood,
          city: city,
          numOfStore: numOfStore,
          id: id,
          houseAge: houseAge,
          price: price,
          title: title,
          location: location,
          totalArea: totalArea,
          description: description,
    imageURLs: imageURLs,
          transactionDate: transactionDate,
          distanceToMRT: distanceToMRT,
        );

  @override
  factory VacantLandProperty.fromJson(Map<String, dynamic> json) => _$VacantLandPropertyFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$VacantLandPropertyToJson(this);
}
