import 'package:bunyan/models/property/property.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Commercial_property.g.dart';

@JsonSerializable()
class CommercialProperty extends Property {
  int? age;
  double? streetWidth;
  String? streetDirection;

  CommercialProperty({
    String? ownerId,
    String? id,
    String? houseAge,
    double? price,
    String? title,
    String? location,
    String? totalArea,
    String? description,
    List<String>? likedBy,
    List<String>? imageURLs,
    String? transactionDate,
    String? distanceToMRT,
    String? numOfStore,
    String? city,
    String? neighborhood,
    String? latitude,
    String? longitude,
    String? nearestHospital,
    String? nearestSchoolOrCollage,
    String? nearestMarket,
    String? nearestMosque,
    String? cityCenter,
    required String name,
    this.age,
    this.streetWidth,
    this.streetDirection,
  }) : super(
          name: name,
          cityCenter: cityCenter,
          ownerId: ownerId,
          nearestMosque: nearestMosque,
          nearestMarket: nearestMarket,
          nearestSchoolOrCollage: nearestSchoolOrCollage,
          nearestHospital: nearestHospital,
          longitude: longitude,
          likedBy: likedBy,
          latitude: latitude,
          neighborhood: neighborhood,
          city: city,
          numOfStore: numOfStore,
          id: id,
          houseAge: houseAge,
          price: price,
          title: title,
          location: location,
          totalArea: totalArea,
          description: description,
          imageURLs: imageURLs,
          transactionDate: transactionDate,
          distanceToMRT: distanceToMRT,
        );

  @override
  factory CommercialProperty.fromJson(Map<String, dynamic> json) =>
      _$CommercialPropertyFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CommercialPropertyToJson(this);
}
