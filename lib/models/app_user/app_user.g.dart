// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$NormalUser _$$NormalUserFromJson(Map<String, dynamic> json) => _$NormalUser(
      id: json['id'] as String?,
      userName: json['userName'] as String?,
      email: json['email'] as String,
      phoneNumber: json['phoneNumber'] as String,
      chats: (json['chats'] as List<dynamic>?)
          ?.map((e) => Chat.fromJson(e as Map<String, dynamic>))
          .toList(),
      favoritesIds: (json['favoritesIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      type: $enumDecode(_$UserTypeEnumMap, json['type']),
      ownedPropertiesIds: (json['ownedPropertiesIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$NormalUserToJson(_$NormalUser instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userName': instance.userName,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'chats': instance.chats,
      'favoritesIds': instance.favoritesIds,
      'type': _$UserTypeEnumMap[instance.type],
      'ownedPropertiesIds': instance.ownedPropertiesIds,
      'runtimeType': instance.$type,
    };

const _$UserTypeEnumMap = {
  UserType.individual: 'individual',
  UserType.business: 'business',
};

_$Chat _$$ChatFromJson(Map<String, dynamic> json) => _$Chat(
      message: (json['message'] as List<dynamic>?)
          ?.map((e) => Message.fromJson(e as Map<String, dynamic>))
          .toList(),
      senderId: json['senderId'] as String?,
      receiverId: json['receiverId'] as String?,
      dateTime: json['dateTime'] == null
          ? null
          : DateTime.parse(json['dateTime'] as String),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$ChatToJson(_$Chat instance) => <String, dynamic>{
      'message': instance.message,
      'senderId': instance.senderId,
      'receiverId': instance.receiverId,
      'dateTime': instance.dateTime?.toIso8601String(),
      'runtimeType': instance.$type,
    };

_$Message _$$MessageFromJson(Map<String, dynamic> json) => _$Message(
      id: json['id'] as String?,
      sender: json['sender'] as String?,
      receiver: json['receiver'] as String?,
      text: json['text'] as String?,
      imageUrl: json['imageUrl'] as String?,
      both: (json['both'] as List<dynamic>?)?.map((e) => e as bool).toList(),
      senderId: json['senderId'] as String?,
      receiverId: json['receiverId'] as String?,
      createdAt: json['createdAt'],
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$MessageToJson(_$Message instance) => <String, dynamic>{
      'id': instance.id,
      'sender': instance.sender,
      'receiver': instance.receiver,
      'text': instance.text,
      'imageUrl': instance.imageUrl,
      'both': instance.both,
      'senderId': instance.senderId,
      'receiverId': instance.receiverId,
      'createdAt': instance.createdAt,
      'runtimeType': instance.$type,
    };
