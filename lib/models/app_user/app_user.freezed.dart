// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'app_user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppUser _$AppUserFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'normalUser':
      return NormalUser.fromJson(json);
    case 'chat':
      return Chat.fromJson(json);
    case 'message':
      return Message.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'AppUser',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
class _$AppUserTearOff {
  const _$AppUserTearOff();

  NormalUser normalUser(
      {required String? id,
      required String? userName,
      required String email,
      required String phoneNumber,
      List<Chat>? chats,
      List<String>? favoritesIds,
      required UserType type,
      List<String>? ownedPropertiesIds}) {
    return NormalUser(
      id: id,
      userName: userName,
      email: email,
      phoneNumber: phoneNumber,
      chats: chats,
      favoritesIds: favoritesIds,
      type: type,
      ownedPropertiesIds: ownedPropertiesIds,
    );
  }

  Chat chat(
      {List<Message>? message,
      String? senderId,
      String? receiverId,
      DateTime? dateTime}) {
    return Chat(
      message: message,
      senderId: senderId,
      receiverId: receiverId,
      dateTime: dateTime,
    );
  }

  Message message(
      {String? id,
      String? sender,
      String? receiver,
      String? text,
      String? imageUrl,
      List<bool>? both,
      String? senderId,
      String? receiverId,
      dynamic createdAt}) {
    return Message(
      id: id,
      sender: sender,
      receiver: receiver,
      text: text,
      imageUrl: imageUrl,
      both: both,
      senderId: senderId,
      receiverId: receiverId,
      createdAt: createdAt,
    );
  }

  AppUser fromJson(Map<String, Object?> json) {
    return AppUser.fromJson(json);
  }
}

/// @nodoc
const $AppUser = _$AppUserTearOff();

/// @nodoc
mixin _$AppUser {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)
        normalUser,
    required TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)
        chat,
    required TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)
        message,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NormalUser value) normalUser,
    required TResult Function(Chat value) chat,
    required TResult Function(Message value) message,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppUserCopyWith<$Res> {
  factory $AppUserCopyWith(AppUser value, $Res Function(AppUser) then) =
      _$AppUserCopyWithImpl<$Res>;
}

/// @nodoc
class _$AppUserCopyWithImpl<$Res> implements $AppUserCopyWith<$Res> {
  _$AppUserCopyWithImpl(this._value, this._then);

  final AppUser _value;
  // ignore: unused_field
  final $Res Function(AppUser) _then;
}

/// @nodoc
abstract class $NormalUserCopyWith<$Res> {
  factory $NormalUserCopyWith(
          NormalUser value, $Res Function(NormalUser) then) =
      _$NormalUserCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String? userName,
      String email,
      String phoneNumber,
      List<Chat>? chats,
      List<String>? favoritesIds,
      UserType type,
      List<String>? ownedPropertiesIds});
}

/// @nodoc
class _$NormalUserCopyWithImpl<$Res> extends _$AppUserCopyWithImpl<$Res>
    implements $NormalUserCopyWith<$Res> {
  _$NormalUserCopyWithImpl(NormalUser _value, $Res Function(NormalUser) _then)
      : super(_value, (v) => _then(v as NormalUser));

  @override
  NormalUser get _value => super._value as NormalUser;

  @override
  $Res call({
    Object? id = freezed,
    Object? userName = freezed,
    Object? email = freezed,
    Object? phoneNumber = freezed,
    Object? chats = freezed,
    Object? favoritesIds = freezed,
    Object? type = freezed,
    Object? ownedPropertiesIds = freezed,
  }) {
    return _then(NormalUser(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      userName: userName == freezed
          ? _value.userName
          : userName // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      chats: chats == freezed
          ? _value.chats
          : chats // ignore: cast_nullable_to_non_nullable
              as List<Chat>?,
      favoritesIds: favoritesIds == freezed
          ? _value.favoritesIds
          : favoritesIds // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as UserType,
      ownedPropertiesIds: ownedPropertiesIds == freezed
          ? _value.ownedPropertiesIds
          : ownedPropertiesIds // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$NormalUser implements NormalUser {
  const _$NormalUser(
      {required this.id,
      required this.userName,
      required this.email,
      required this.phoneNumber,
      this.chats,
      this.favoritesIds,
      required this.type,
      this.ownedPropertiesIds,
      String? $type})
      : $type = $type ?? 'normalUser';

  factory _$NormalUser.fromJson(Map<String, dynamic> json) =>
      _$$NormalUserFromJson(json);

  @override
  final String? id;
  @override
  final String? userName;
  @override
  final String email;
  @override
  final String phoneNumber;
  @override
  final List<Chat>? chats;
  @override
  final List<String>? favoritesIds;
  @override
  final UserType type;
  @override
  final List<String>? ownedPropertiesIds;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'AppUser.normalUser(id: $id, userName: $userName, email: $email, phoneNumber: $phoneNumber, chats: $chats, favoritesIds: $favoritesIds, type: $type, ownedPropertiesIds: $ownedPropertiesIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NormalUser &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.userName, userName) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality()
                .equals(other.phoneNumber, phoneNumber) &&
            const DeepCollectionEquality().equals(other.chats, chats) &&
            const DeepCollectionEquality()
                .equals(other.favoritesIds, favoritesIds) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.ownedPropertiesIds, ownedPropertiesIds));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(userName),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(phoneNumber),
      const DeepCollectionEquality().hash(chats),
      const DeepCollectionEquality().hash(favoritesIds),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(ownedPropertiesIds));

  @JsonKey(ignore: true)
  @override
  $NormalUserCopyWith<NormalUser> get copyWith =>
      _$NormalUserCopyWithImpl<NormalUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)
        normalUser,
    required TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)
        chat,
    required TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)
        message,
  }) {
    return normalUser(id, userName, email, phoneNumber, chats, favoritesIds,
        type, ownedPropertiesIds);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
  }) {
    return normalUser?.call(id, userName, email, phoneNumber, chats,
        favoritesIds, type, ownedPropertiesIds);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
    required TResult orElse(),
  }) {
    if (normalUser != null) {
      return normalUser(id, userName, email, phoneNumber, chats, favoritesIds,
          type, ownedPropertiesIds);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NormalUser value) normalUser,
    required TResult Function(Chat value) chat,
    required TResult Function(Message value) message,
  }) {
    return normalUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
  }) {
    return normalUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
    required TResult orElse(),
  }) {
    if (normalUser != null) {
      return normalUser(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NormalUserToJson(this);
  }
}

abstract class NormalUser implements AppUser {
  const factory NormalUser(
      {required String? id,
      required String? userName,
      required String email,
      required String phoneNumber,
      List<Chat>? chats,
      List<String>? favoritesIds,
      required UserType type,
      List<String>? ownedPropertiesIds}) = _$NormalUser;

  factory NormalUser.fromJson(Map<String, dynamic> json) =
      _$NormalUser.fromJson;

  String? get id;
  String? get userName;
  String get email;
  String get phoneNumber;
  List<Chat>? get chats;
  List<String>? get favoritesIds;
  UserType get type;
  List<String>? get ownedPropertiesIds;
  @JsonKey(ignore: true)
  $NormalUserCopyWith<NormalUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatCopyWith<$Res> {
  factory $ChatCopyWith(Chat value, $Res Function(Chat) then) =
      _$ChatCopyWithImpl<$Res>;
  $Res call(
      {List<Message>? message,
      String? senderId,
      String? receiverId,
      DateTime? dateTime});
}

/// @nodoc
class _$ChatCopyWithImpl<$Res> extends _$AppUserCopyWithImpl<$Res>
    implements $ChatCopyWith<$Res> {
  _$ChatCopyWithImpl(Chat _value, $Res Function(Chat) _then)
      : super(_value, (v) => _then(v as Chat));

  @override
  Chat get _value => super._value as Chat;

  @override
  $Res call({
    Object? message = freezed,
    Object? senderId = freezed,
    Object? receiverId = freezed,
    Object? dateTime = freezed,
  }) {
    return _then(Chat(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as List<Message>?,
      senderId: senderId == freezed
          ? _value.senderId
          : senderId // ignore: cast_nullable_to_non_nullable
              as String?,
      receiverId: receiverId == freezed
          ? _value.receiverId
          : receiverId // ignore: cast_nullable_to_non_nullable
              as String?,
      dateTime: dateTime == freezed
          ? _value.dateTime
          : dateTime // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$Chat implements Chat {
  const _$Chat(
      {this.message,
      this.senderId,
      this.receiverId,
      this.dateTime,
      String? $type})
      : $type = $type ?? 'chat';

  factory _$Chat.fromJson(Map<String, dynamic> json) => _$$ChatFromJson(json);

  @override
  final List<Message>? message;
  @override
  final String? senderId;
  @override
  final String? receiverId;
  @override
  final DateTime? dateTime;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'AppUser.chat(message: $message, senderId: $senderId, receiverId: $receiverId, dateTime: $dateTime)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Chat &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality().equals(other.senderId, senderId) &&
            const DeepCollectionEquality()
                .equals(other.receiverId, receiverId) &&
            const DeepCollectionEquality().equals(other.dateTime, dateTime));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(senderId),
      const DeepCollectionEquality().hash(receiverId),
      const DeepCollectionEquality().hash(dateTime));

  @JsonKey(ignore: true)
  @override
  $ChatCopyWith<Chat> get copyWith =>
      _$ChatCopyWithImpl<Chat>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)
        normalUser,
    required TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)
        chat,
    required TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)
        message,
  }) {
    return chat(this.message, senderId, receiverId, dateTime);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
  }) {
    return chat?.call(this.message, senderId, receiverId, dateTime);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
    required TResult orElse(),
  }) {
    if (chat != null) {
      return chat(this.message, senderId, receiverId, dateTime);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NormalUser value) normalUser,
    required TResult Function(Chat value) chat,
    required TResult Function(Message value) message,
  }) {
    return chat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
  }) {
    return chat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
    required TResult orElse(),
  }) {
    if (chat != null) {
      return chat(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$ChatToJson(this);
  }
}

abstract class Chat implements AppUser {
  const factory Chat(
      {List<Message>? message,
      String? senderId,
      String? receiverId,
      DateTime? dateTime}) = _$Chat;

  factory Chat.fromJson(Map<String, dynamic> json) = _$Chat.fromJson;

  List<Message>? get message;
  String? get senderId;
  String? get receiverId;
  DateTime? get dateTime;
  @JsonKey(ignore: true)
  $ChatCopyWith<Chat> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageCopyWith<$Res> {
  factory $MessageCopyWith(Message value, $Res Function(Message) then) =
      _$MessageCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String? sender,
      String? receiver,
      String? text,
      String? imageUrl,
      List<bool>? both,
      String? senderId,
      String? receiverId,
      dynamic createdAt});
}

/// @nodoc
class _$MessageCopyWithImpl<$Res> extends _$AppUserCopyWithImpl<$Res>
    implements $MessageCopyWith<$Res> {
  _$MessageCopyWithImpl(Message _value, $Res Function(Message) _then)
      : super(_value, (v) => _then(v as Message));

  @override
  Message get _value => super._value as Message;

  @override
  $Res call({
    Object? id = freezed,
    Object? sender = freezed,
    Object? receiver = freezed,
    Object? text = freezed,
    Object? imageUrl = freezed,
    Object? both = freezed,
    Object? senderId = freezed,
    Object? receiverId = freezed,
    Object? createdAt = freezed,
  }) {
    return _then(Message(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      sender: sender == freezed
          ? _value.sender
          : sender // ignore: cast_nullable_to_non_nullable
              as String?,
      receiver: receiver == freezed
          ? _value.receiver
          : receiver // ignore: cast_nullable_to_non_nullable
              as String?,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String?,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      both: both == freezed
          ? _value.both
          : both // ignore: cast_nullable_to_non_nullable
              as List<bool>?,
      senderId: senderId == freezed
          ? _value.senderId
          : senderId // ignore: cast_nullable_to_non_nullable
              as String?,
      receiverId: receiverId == freezed
          ? _value.receiverId
          : receiverId // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$Message implements Message {
  const _$Message(
      {this.id,
      this.sender,
      this.receiver,
      this.text,
      this.imageUrl,
      this.both,
      this.senderId,
      this.receiverId,
      this.createdAt,
      String? $type})
      : $type = $type ?? 'message';

  factory _$Message.fromJson(Map<String, dynamic> json) =>
      _$$MessageFromJson(json);

  @override
  final String? id;
  @override
  final String? sender;
  @override
  final String? receiver;
  @override
  final String? text;
  @override
  final String? imageUrl;
  @override
  final List<bool>? both;
  @override
  final String? senderId;
  @override
  final String? receiverId;
  @override
  final dynamic createdAt;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'AppUser.message(id: $id, sender: $sender, receiver: $receiver, text: $text, imageUrl: $imageUrl, both: $both, senderId: $senderId, receiverId: $receiverId, createdAt: $createdAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is Message &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.sender, sender) &&
            const DeepCollectionEquality().equals(other.receiver, receiver) &&
            const DeepCollectionEquality().equals(other.text, text) &&
            const DeepCollectionEquality().equals(other.imageUrl, imageUrl) &&
            const DeepCollectionEquality().equals(other.both, both) &&
            const DeepCollectionEquality().equals(other.senderId, senderId) &&
            const DeepCollectionEquality()
                .equals(other.receiverId, receiverId) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(sender),
      const DeepCollectionEquality().hash(receiver),
      const DeepCollectionEquality().hash(text),
      const DeepCollectionEquality().hash(imageUrl),
      const DeepCollectionEquality().hash(both),
      const DeepCollectionEquality().hash(senderId),
      const DeepCollectionEquality().hash(receiverId),
      const DeepCollectionEquality().hash(createdAt));

  @JsonKey(ignore: true)
  @override
  $MessageCopyWith<Message> get copyWith =>
      _$MessageCopyWithImpl<Message>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)
        normalUser,
    required TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)
        chat,
    required TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)
        message,
  }) {
    return message(id, sender, receiver, text, imageUrl, both, senderId,
        receiverId, createdAt);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
  }) {
    return message?.call(id, sender, receiver, text, imageUrl, both, senderId,
        receiverId, createdAt);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(
            String? id,
            String? userName,
            String email,
            String phoneNumber,
            List<Chat>? chats,
            List<String>? favoritesIds,
            UserType type,
            List<String>? ownedPropertiesIds)?
        normalUser,
    TResult Function(List<Message>? message, String? senderId,
            String? receiverId, DateTime? dateTime)?
        chat,
    TResult Function(
            String? id,
            String? sender,
            String? receiver,
            String? text,
            String? imageUrl,
            List<bool>? both,
            String? senderId,
            String? receiverId,
            dynamic createdAt)?
        message,
    required TResult orElse(),
  }) {
    if (message != null) {
      return message(id, sender, receiver, text, imageUrl, both, senderId,
          receiverId, createdAt);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NormalUser value) normalUser,
    required TResult Function(Chat value) chat,
    required TResult Function(Message value) message,
  }) {
    return message(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
  }) {
    return message?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NormalUser value)? normalUser,
    TResult Function(Chat value)? chat,
    TResult Function(Message value)? message,
    required TResult orElse(),
  }) {
    if (message != null) {
      return message(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MessageToJson(this);
  }
}

abstract class Message implements AppUser {
  const factory Message(
      {String? id,
      String? sender,
      String? receiver,
      String? text,
      String? imageUrl,
      List<bool>? both,
      String? senderId,
      String? receiverId,
      dynamic createdAt}) = _$Message;

  factory Message.fromJson(Map<String, dynamic> json) = _$Message.fromJson;

  String? get id;
  String? get sender;
  String? get receiver;
  String? get text;
  String? get imageUrl;
  List<bool>? get both;
  String? get senderId;
  String? get receiverId;
  dynamic get createdAt;
  @JsonKey(ignore: true)
  $MessageCopyWith<Message> get copyWith => throw _privateConstructorUsedError;
}
