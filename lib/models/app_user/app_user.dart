import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_user.freezed.dart';

part 'app_user.g.dart';

@freezed
class AppUser with _$AppUser {
  const factory AppUser.normalUser({
    required String? id,
    required String? userName,
    required String email,
    required String phoneNumber,
    List<Chat>? chats,
    List<String>? favoritesIds,
    required UserType type,
    List<String>? ownedPropertiesIds,
  }) = NormalUser;

  const factory AppUser.chat({
    List<Message>? message,
    String? senderId,
    String? receiverId,
    DateTime? dateTime,
  }) = Chat;

  const factory AppUser.message({
    String? id,
    String? sender,
    String? receiver,
    String? text,
    String? imageUrl,
    List<bool>? both,
    String? senderId,
    String? receiverId,
    dynamic createdAt,
  }) = Message;

  factory AppUser.fromJson(Map<String, dynamic> json) => _$AppUserFromJson(json);
}
//flutter pub run build_runner build --delete-conflicting-outputs
