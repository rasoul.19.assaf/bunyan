part of 'commercial_properties_cubit.dart';

@immutable
abstract class CommercialPropertiesState {
  List<CommercialProperty> commercialProperties = [];
}

class CommercialPropertiesInitial extends CommercialPropertiesState {}

class CommercialPropertiesLoading extends CommercialPropertiesState {}

class CommercialPropertiesLoaded extends CommercialPropertiesState {}
