import 'package:bloc/bloc.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/property/Residential_property.dart';
import 'package:meta/meta.dart';

part 'residential_properties_state.dart';

class ResidentialPropertiesCubit extends Cubit<ResidentialPropertiesState> {
  ResidentialPropertiesCubit() : super(ResidentialPropertiesInitial()) {
    load();
  }

  void load() async {
    print("load");
    emit(ResidentialPropertiesLoading());

    ResidentialPropertiesLoaded newState = ResidentialPropertiesLoaded();
    newState.residentialProperties =
        await FirestoreRepository.instance.getResidentialProperties();
    print("loaded");

    emit(newState);
  }
}
