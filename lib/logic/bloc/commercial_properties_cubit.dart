import 'package:bloc/bloc.dart';
import 'package:bunyan/models/property/property.dart';
import 'package:meta/meta.dart';

import '../../data/repositories/firestore_repository.dart';
import '../../models/property/Commercial_property.dart';

part 'commercial_properties_state.dart';

class CommercialPropertiesCubit extends Cubit<CommercialPropertiesState> {
  CommercialPropertiesCubit() : super(CommercialPropertiesInitial()) {
    load();
  }

  void load() async {
    print("load");
    emit(CommercialPropertiesLoading());

    CommercialPropertiesLoaded newState = CommercialPropertiesLoaded();
    newState.commercialProperties =
        await FirestoreRepository.instance.getCommercialProperties();
    print("loaded");

    emit(newState);
  }
}
