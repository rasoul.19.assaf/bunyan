part of 'residential_properties_cubit.dart';

@immutable
abstract class ResidentialPropertiesState {
  List<ResidentialProperty> residentialProperties = [];
}

class ResidentialPropertiesInitial extends ResidentialPropertiesState {}

class ResidentialPropertiesLoading extends ResidentialPropertiesState {}

class ResidentialPropertiesLoaded extends ResidentialPropertiesState {}
