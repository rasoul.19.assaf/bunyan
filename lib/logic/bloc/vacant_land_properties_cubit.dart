import 'package:bloc/bloc.dart';
import 'package:bunyan/data/repositories/firestore_repository.dart';
import 'package:bunyan/models/property/VacantLand_property.dart';
import 'package:meta/meta.dart';

part 'vacant_land_properties_state.dart';

class VacantLandPropertiesCubit extends Cubit<VacantLandPropertiesState> {
  VacantLandPropertiesCubit() : super(VacantLandPropertiesInitial()) {
    load();
  }

  void load() async {
    print("load");
    emit(VacantLandPropertiesLoading());

    VacantLandPropertiesLoaded newState = VacantLandPropertiesLoaded();
    newState.vacantLandProperties =
        await FirestoreRepository.instance.getVacantLandProperties();
    print("loaded");

    emit(newState);
  }
}
