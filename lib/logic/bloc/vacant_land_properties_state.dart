part of 'vacant_land_properties_cubit.dart';

@immutable
abstract class VacantLandPropertiesState {
  List<VacantLandProperty> vacantLandProperties = [];
}

class VacantLandPropertiesInitial extends VacantLandPropertiesState {}

class VacantLandPropertiesLoading extends VacantLandPropertiesState {}

class VacantLandPropertiesLoaded extends VacantLandPropertiesState {}
